import Input from './input.js';
import TokenRequest from './TokenRequest.js';
import Modal from './modal.js';

class CreateVisit extends Modal {
    constructor(id, classTest) {
        super(id, classTest);
        this.select = document.createElement('select');
        this.optionDefault = document.createElement('option');
        this.optionDentist = document.createElement('option');
        this.optionTherapist = document.createElement('option');
        this.chooseDoctorP = document.createElement('p');
        this.optionCardiologist = document.createElement('option');
        this.optionDoctorSelect = document.createElement('select');
        this.optionSelectLow = document.createElement('Low');
        this.optionSelectNormal = document.createElement('Normal');
        this.optionSelectHigh = document.createElement('High');
        this.options = this.checkForDoubleSelect(
            this.priority,
            'Low',
            'Normal',
            'High'
        );
        this.optionsStatus = this.checkForPrioritySelect(
            this.status,
            'Open',
            'Done'
        );
        // this.autoForm = document.createElement('form');
    }
    checkForPrioritySelect(currentSelector, ...args) {
        return [...args].filter(el => el !== currentSelector);
    }
    checkForDoubleSelect(currentSelector, ...args) {
        return [...args].filter(el => el !== currentSelector);
    }

    render() {
        super.render(root);
        const autoForm = document.createElement('form');
        autoForm.classList = 'visit_form';
        this.optionDoctorSelect.id = 'inputUrgencyDentist';
        this.optionDoctorSelect.classList.add('form-select');
        this.chooseDoctorP.innerText =
            'Select the doctor you would like to visit:';
        this.chooseDoctorP.classList.add('choose-doctor');
        this.optionDefault.innerText = 'choose doctor:';
        this.select.id = 'select-doctor';
        this.select.classList.add('form-select');
        //this.select.setAttribute('disabled', 'disabled');
        this.optionDentist.innerText = 'Dentist';
        this.optionTherapist.innerText = 'Therapist';
        this.optionCardiologist.innerText = 'Cardiologist';
        this.optionDentist.value = '1';
        this.optionTherapist.value = '2';
        this.optionCardiologist.value = '3';
        this.optionDefault.selected = true;
        this.select.append(
            this.optionDefault,
            this.optionDentist,
            this.optionTherapist,
            this.optionCardiologist
        );
        this.optionDoctorSelect.append(
            this.optionSelectLow,
            this.optionSelectNormal,
            this.optionSelectHigh
        );

        const purposeOfVisit = new Input(
            'text',
            'purposeOfVisit',
            'required',
            'idPurposeOfVisit',
            ['classPurposeOfVisit'],
            'the purpose of the visit',
            'wtf'
        ).render();
        const descriptionOfVisit = new Input(
            'text',
            'descriptionOfVisit',
            'required',
            'idDescriptionOfVisit',
            ['classDescriptionOfVisit'],
            'description of the visit',
            'wtf'
        ).render();
        const firstName = new Input(
            'text',
            'firstName',
            'required',
            'firstName',
            ['firstName'],
            'firstName',
            'wtf'
        ).render();
        const secondName = new Input(
            'text',
            'secondName',
            'required',
            'secondName',
            ['secondName'],
            'secondName',
            'wtf'
        ).render();
        this.modalContent.append(autoForm);
        autoForm.append(
            this.select,
            purposeOfVisit,
            descriptionOfVisit,
            firstName,
            secondName
        );
    }
}
const createVisit = new CreateVisit('modalVisit', ['modalVisit']);
export default createVisit;
