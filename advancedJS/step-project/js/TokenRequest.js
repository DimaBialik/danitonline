export default class TokenRequest {
    constructor(email, password) {
        this.email = email;
        this.password = password;
    }
    requestToken() {
        fetch('https://ajax.test-danit.com/api/v2/cards/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: this.email, password: this.password })
        })
            .then(response => response.text())
            .then(token => localStorage.setItem('token', token));
    }
}

// const tokenRequest= new ModalRequest(email,password)
// export default tokenRequest
