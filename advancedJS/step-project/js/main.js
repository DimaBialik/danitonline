import authorization from './CreateLogin.js';
import createVisit from './CreateVisit.js';

//console.log(authorization);

class App {
    constructor() {
        this.root = document.querySelector('#root');
        this.headers = document.createElement('div');
    }

    renderApp() {}
    renderHeaders() {
        const headersMain = document.createElement('div');
        const logo = document.createElement('div');
        const logoImg = document.createElement('p');
        logoImg.innerHTML = 'LOGO';
        const btnLogin = document.createElement('button');
        const btnCreateVisit = document.createElement('button');
        btnLogin.innerHTML = 'Login';
        btnCreateVisit.innerHTML = 'Create Visit';
        logo.append(logoImg);
        headersMain.append(logo, btnLogin, btnCreateVisit);

        this.headers.append(headersMain);
        this.root.append(this.headers);
        btnLogin.addEventListener('click', e => {
            e.stopPropagation();
            authorization.render(this.root);
            authorization.openModal();
        });
        btnCreateVisit.addEventListener('click', e => {
            e.stopPropagation();
            createVisit.render(this.root);
            createVisit.openModal();
        });
    }
    renderFiltres() {
        const headersFilter = document.createElement('div');
        const filterInput = document.createElement('input');
    }
    modalLogin() {}
}
const app = new App();
app.renderHeaders();
//app. modalRequest()
