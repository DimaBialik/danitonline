

export default  class Input {
    constructor(
        type,
        name,
        required,
        id,
        classes,
        placeholder,
        errorText,
        value
    ) {
        this.type = type;
        this.name = name;
        this.required = required;
        this.id = id;
        this.classes = classes;
        this.placeholder = placeholder;
        this.errorText = errorText;
        this.value = value;
    }
    render() {
        const input = document.createElement("input");
        input.setAttribute("type", this.type);
        input.setAttribute("name", this.name);
        input.setAttribute("required", this.required);
        input.setAttribute("id", this.id);
        input.classList.add(...this.classes);
        input.setAttribute("placeholder", this.placeholder);

        if (input.type === "submit") {
            input.setAttribute("value", "submit");
        }
        return input;
    }
    handleBlur() {
        const p = document.createElement("p");
        p.innerText = this.errorText;
    }
}
