import Input from './input.js';
import TokenRequest from './TokenRequest.js';
import Modal from './modal.js';

class Authorization extends Modal {
    constructor(id, classTest) {
        super(id, classTest);
        //this.autoForm = document.createElement('form');
    }
    render() {
        super.render(root);

        const autoForm = document.createElement('form');

        autoForm.classList = 'login_form';
        // autoForm.action = '#';

        const inputLogin = new Input(
            'text',
            'login',
            'required',
            'idInputLogin',
            ['classesInputLogin', 'classesInputLogin1'],
            'u login',
            'wtf'
        ).render();

        const inputPassword = new Input(
            'password',
            'password',
            'required',
            'idInputPassword',
            ['classesInputPassword'],
            'u password',
            'wtf'
        ).render();
        const inputSubmit = new Input(
            'submit',
            'submit',
            'required',
            'idInputSubmit',
            ['classesInputSubmit'],
            'submit',
            'wtf'
        ).render();
        inputSubmit.addEventListener('click', e => {
            e.preventDefault();

            const tokenRequest = new TokenRequest(
                inputLogin.value,
                inputPassword.value
            );
            tokenRequest.requestTtoken();

            this.modalContainer.remove();
        });
        this.modalContent.append(autoForm);
        autoForm.append(inputLogin, inputPassword, inputSubmit);
    }
}

const authorization = new Authorization('modal11', ['modal']);
export default authorization;
