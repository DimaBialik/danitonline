export default class Modal {
    constructor(id, classList) {
        this.id = id;
        this.classList = classList;
    }
    render(root) {
        this.modalContainer = document.createElement('div');
        this.modalContent = document.createElement('div');
        this.modalClose = document.createElement('span');

        this.modalContainer.id = this.id;
        this.modalContainer.classList.add(...this.classList);

        this.modalContent.className = 'modal-content';

        this.modalClose.className = 'close';
        this.modalClose.style.cursor = 'pointer';
        this.modalClose.innerHTML = '&times;';
        this.modalClose.addEventListener('click', this.closeModal);
        let modalOpen = true;
        document.addEventListener('click', e => {
            e.preventDefault();

            const target = e.target;
            const its_modal =
                target == this.modalContainer ||
                this.modalContainer.contains(target);
            const its_btnModal = target == this.modalClose;
            const menu_is_active =
                this.modalContainer.classList.contains('active');

            if (!its_modal && !its_btnModal && menu_is_active) {
                if (!modalOpen) {
                    this.openModal();
                    modalOpen = true;
                } else {
                    this.closeModal();
                    modalOpen = false;
                }
            }
        });
        this.modalContent.append(this.modalClose);
        this.modalContainer.append(this.modalContent);
        root.append(this.modalContainer);
    }
    openModal = () => {
        this.modalContainer.classList.add('active');
    };

    closeModal = () => {
        // this.modalContainer.classList.remove('active');
        this.modalContainer.remove();
    };
}
