// const BASE_URI = 'http://localhost:3000';

// const templateList = document.getElementById('posts-list').content;
// console.log(templateList);
// const list = templateList.querySelector('ul');
// console.log(list);

// task 1
const root = document.querySelector('#root');
const url = 'http://localhost:3000';
class Requests {
    constructor(url) {
        this.url = url;
    }
    post(json) {
        return fetch(`${this.url}/posts`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: json
        });
    }
}

const requests = new Requests(url);

class Form {
    constructor(root) {
        this.root = root;
    }
    render() {
        const formFragment = document.querySelector('#create-post').content;
        const form = formFragment.querySelector('form');
        this.root.append(formFragment);
        form.addEventListener('submit', e => {
            e.preventDefault();
            this.formCheck(e.target);
        });
    }
    formCheck(form) {
        const formElements = [...form.elements].filter(
            ({ type }) => type !== 'submit'
        );
        const formElementsNotEmpty = formElements.filter(el => {
            if (el.value.trim()) {
                return el;
            }
        });
        if (formElements.length === formElementsNotEmpty.length) {
            const objForm = {};
            formElementsNotEmpty.forEach(({ id, value }) => {
                objForm[id] = value;
            });
            const jsonForm = JSON.stringify(objForm);
            requests
                .post(jsonForm)
                .then(response => response.json())
                .then(data => {
                    form.reset();
                    const postsList =
                        document.querySelector('#posts-list').content;
                    const list = postsList.querySelector('ul');
                    const listItem = postsList.querySelector('li');
                    const { title, intro, text, createAt, author } = data;
                    listItem.innerHTML = `
                    <p>${title}</p>
                    <p>${intro}</p>
                    <p>${text}</p>
                    <p>${createAt}</p>
                    <p>${author}</p>
                    `;
                    list.append(listItem);
                    this.root.append(list);
                });
        } else {
            alert('Entry content');
        }
    }
}
const form = new Form(root);
form.render();

// task 2
