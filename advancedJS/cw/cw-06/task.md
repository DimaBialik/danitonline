/_
TASK 1
Создать класс, который отправляет AJAX запрос по адресу
https://ajax.test-danit.com/api/swapi/films/, получает список всех фильмов и
выводит названия на страницу в root.
_/
/_
TASK 2
Получить все планеты из киновселенной Star Wars
https://ajax.test-danit.com/api-pages/swapi.html
Вывести планеты списком в порядке уменьшения радиуса планеты. Реализовать через
класс.
TASK 3
Получить все фильмы из киновселенной Star Wars
https://ajax.test-danit.com/api-pages/swapi.html
Вывести списком названия фильмов и планеты, которые относятся к фильму.
Реализовать через класс.
_/
