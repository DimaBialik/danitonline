// const url = 'https://ajax.test-danit.com/api/swapi/films/';

// const root = document.querySelector('#root');
// class Films {
//     constructor(url, root) {
//         this.url = url;
//         this.root = root;
//     }
//     dispatch() {
//         const getPlanets = new Promise((resolve, reject) => {
//             const xhr = new XMLHttpRequest();
//             xhr.open('GET', `${this.url}`);
//             xhr.send();

//             xhr.onload = () => resolve(xhr.response);
//             xhr.onerror = () => reject(e);
//         });

//         getPlanets
//             .then(response => {
//                 const data = JSON.parse(response);
//                 //console.log(data);
//                 this.render(data);
//             })
//             .catch(e => {
//                 console.error(e);
//             });
//     }

//     render(items) {
//         var fragment = document.createDocumentFragment();
//         items.forEach(el => {
//             const pFilm = document.createElement('p');
//             pFilm.innerText = el.name;
//             fragment.append(pFilm);
//         });
//         this.root.append(fragment);
//     }
// }
// const films = new Films(url, root);
// films.dispatch();

const BASE_URL = 'https://ajax.test-danit.com/api/swapi/';
const entity = 'planets';
const root = document.querySelector('#root');

class Planets {
    constructor(BASE_URL, entity, root) {
        this.BASE_URL = BASE_URL;
        this.entity = entity;
        this.root = root;
    }
    dispatch() {
        const getPlanets = new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open('GET', `${this.BASE_URL}${this.entity}`);
            xhr.send();

            xhr.onload = () => resolve(xhr.response);
            xhr.onerror = () => reject(e);
        });

        getPlanets
            .then(response => {
                const data = JSON.parse(response);
                data.sort(function (a, b) {
                    if (a.diameter < b.diameter) {
                        return 1;
                    } else {
                        return -1;
                    }
                });
                this.render(data);
            })
            .catch(e => {
                console.error(e);
            });
    }
    render(items) {
        const fragment = document.createDocumentFragment();

        items.forEach(el => {
            const pFilm = document.createElement('p');
            pFilm.innerText = `${el.name} - ${el.diameter}`;
            fragment.append(pFilm);
        });
        this.root.append(fragment);
    }
}

const planets = new Planets(BASE_URL, entity, root);
planets.dispatch();
