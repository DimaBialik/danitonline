// function createUser(name) {
//     return {
//         name,
//     };
// }

// const user = createUser("Uasya");
// console.log(user);

// Number(), new String()

// function User(name) {
//     this.name = name;
//     this.showName = function () {
//         return this.name;
//     };

//     // function newAge() {
//     //     console.log("123");
//     // }
// }

// // console.log(User);

// const user = new User("Uasya");
// const user2 = new User("Gosha");
// const user3 = new User("Vitaliy");

// // console.log(user);
// // console.log(user2.showName());

// const user4 = {
//     name: "Slava",
// };

// // user4.showName = user.showName;

// console.log(user.showName.call(user4));
// console.log(user.showName.apply(user4));
// console.log(user.showName.bind(user4)());

// TASK 1
// function Question(question,answer){
//     this.question = question;
//     this.answer = answer;
//     this.render = function (root){
//         const questionText = document.createElement(`a`);
//         const questionAnswer = document.createElement('p');
//         questionAnswer.innerText = answer;
//         questionText.classList.add('question');
//         questionText.innerText = question;
//         let counter = 0;
//         questionText.addEventListener('click', (event) =>{
//             event.preventDefault();
//             questionText.insertAdjacentElement("afterend", questionAnswer)
//              counter += 1;
//             if (counter % 2 === 0){
//                 questionAnswer.remove();
//             }
//         })
//         root.insertAdjacentElement("beforeend", questionText);
//     }
// }

// const questionText = "Девиз дома Баратеонов";
// const questionAnswer = "Нам ярость!";
// const question1 = new Question(questionText,questionAnswer)
// const question2 = new Question('Yes', 'No')
// question1.render(document.querySelector('#root'));
// question2.render(document.querySelector('#root'));

// TASK 2

// function Stopwatch(container) {
//     this._time = 0;
//     let timerID = 0;
//     this.start = function () {
//         container.textContent = this.getTime();
//         if (timerID === 0) {
//             timerID = setInterval(() => {
//                 console.log(this)
//                 this.setTime(this.getTime() + 1);
//                 container.textContent = this.getTime();
//             }, 1000);
//         }
//     }
//     this.stop = function () {
//         clearInterval(timerID);
//         timerID = 0;
//     }
//     this.reset = function (){
//         this.setTime(0);
//         container.textContent = this.getTime();
//     }
//     this.setTime = function (number) {
//         if (number >= 0) {
//             this._time = number;
//             return {status: "success",};
//         }
//         return {
//             status: "error",
//             message: "argument must be positive integer"
//         }
//     }
//     this.getTime = function () {
//         return this._time;
//     }
// }

// const startBtn = document.getElementById('start-time');
// const stopBtn = document.getElementById('stop-time');
// const resetBtn = document.getElementById('reset-time');

// const stopWatchContainer = document.getElementById('time');
// const stopwatch = new Stopwatch(stopWatchContainer);
// console.dir(stopwatch);

// startBtn.addEventListener('click', stopwatch.start.bind(stopwatch));
// stopBtn.addEventListener('click', stopwatch.stop.bind(stopwatch));
// resetBtn.addEventListener('click', stopwatch.reset.bind(stopwatch));

// TASK 3

function Modal(id, className, text) {
    this.id = id;
    this.className = className;
    this.text = text;

    this.render = function (root) {
        this.modalWindow = document.createElement("div");
        const modalContent = document.createElement("div");
        const modalClose = document.createElement("span");
        const modalText = document.createElement("p");

        this.modalWindow.classList.add(...this.className);
        this.modalWindow.id = this.id;

        modalContent.classList.add("modal-content");
        this.modalWindow.append(modalContent);

        modalClose.classList.add("close");
        modalClose.innerHTML = "&times;";
        modalClose.addEventListener("click", this.closeModal);
        modalContent.append(modalClose);

        modalText.textContent = this.text;
        modalContent.append(modalText);

        root.append(this.modalWindow);
    };

    this.openModal = () => {
        this.modalWindow.classList.add("active");
    };

    this.closeModal = () => {
        this.modalWindow.classList.remove("active");
    };
}

const modal = new Modal(1, ["modal", "b", "c"], 2);

console.log(modal);
modal.render(document.querySelector("#root"));

const btn = document.querySelector("#myBtn");
btn.addEventListener("click", modal.openModal);
