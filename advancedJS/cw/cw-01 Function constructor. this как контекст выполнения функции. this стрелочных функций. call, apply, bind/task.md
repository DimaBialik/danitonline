##TASK 1

Напишите функцию-конструктор, создающую объект, реализующий такой функционал: у
нас на странице есть вопрос. При первом клике на него под ним открывается ответ
на вопрос. При повтором - ответ прячется. Разметку вы найдете в файле.

         <a href="" class="question">Девиз дома Баратеонов</a>
         <div id="root"></div>

    const questionText = "Девиз дома Баратеонов"; const questionAnswer = "Нам
    ярость!";

##TASK 2

Создайте функцию-конструтор объекта `stopwatch` согласно описанию:

1. Свойства объекта: - `_time`: время; - `container`: ссылка на DOM-элемент,
   внутри которого нужно выводить время.
2. Методы объекта: - `start`, `stop`, `reset`, работающие с его свойствами. -
   `setTime` и `getTime`. `setTime`будет получать новое значение времени в
   качестве аргумента, и проверять, является ли оно положительным целым числом.
   Если да - то свойству `_time` будет присвоено значение аргумента, переданного
   в метод `setTime` и метод вернет ответ объект вида: `{ status: "success", }`

Если же аргумент не удовлетворяет критериям, то возвращать объект вида:

```
{
status: "error",
message: "argument must be positive integer"
}
```

Метод же `getTime` будет просто возвращать значение свойства `_time` для
использования его вне методов объектов.

    <p id="time"></p>
    <button id="start-time">Start</button>
    <button id="stop-time">Stop</button>
    <button id="reset-time">Reset</button>

function Stopwatch(container) {

}

const startBtn = document.getElementById('start-time'); const stopBtn =
document.getElementById('stop-time'); const resetBtn =
document.getElementById('reset-time');

const stopWatchContainer = document.getElementById('time'); const stopwatch =
new Stopwatch(stopWatchContainer);

startBtn.addEventListener('click', stopwatch.start.bind(stopwatch));
stopBtn.addEventListener('click', stopwatch.stop.bind(stopwatch));
resetBtn.addEventListener('click', stopwatch.reset.bind(stopwatch));

##TASK 3

Задание: напишите функцию-констукртор Modal, которая будет создавать объект,
описывающий всплывающее окно. Параметры объекта: - id всплывающего окна; -
классы всплывающего окна; - текст внутри тега p;

У объекта должно быть 3 метода: render, который возвращает DOM-элемент
всплывающего окна с такой разметкой:

      <div id="idОкна" class="классыОкна">
      <div class="modal-content">
      <span class="close">&times;</span>
      <p>ТекстОкна</p>
      </div>
      </div>

openModal, который открывает окно (его нужно использовать как обработчик click
для button с id="myBtn"); closeModal - который закрывает окно при клике на
крестик (span с классом close) внутри окна

Код для вставки в HTML

      <style>
      body {
      font-family: Arial, Helvetica, sans-serif;
      }

      /* The Modal (background) */
      .modal {
      display: none;
      /* Hidden by default */
      position: fixed;
      /* Stay in place */
      z-index: 1;
      /* Sit on top */
      padding-top: 100px;
      /* Location of the box */
      left: 0;
      top: 0;
      width: 100%;
      /* Full width */
      height: 100%;
      /* Full height */
      overflow: auto;
      /* Enable scroll if needed */
      background-color: rgb(0, 0, 0);
      /* Fallback color */
      background-color: rgba(0, 0, 0, 0.4);
      /* Black w/ opacity */
      }

      .modal.active {
      display: block;
      }

      /* Modal Content */
      .modal-content {
      background-color: #fefefe;
      margin: auto;
      padding: 20px;
      border: 1px solid #888;
      width: 80%;
      }

      /* The Close Button */
      .close {
      color: #aaaaaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
      }

      .close:hover,
      .close:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
      }

      </style>



      <div id="root"></div>
      <button id="myBtn">Open Modal</button>

TASK 4

Возьмите код из предыдущей задачи и на основе функции Modal создайте два обьекта
с разным содержимым:

1.  Вплывающее окно с формой регистрации. Его HTML-разметка будет выглядть так:

        <div id="idОкна" class="классыОкна">
        <div class="modal-content">
        <span class="close">&times;</span>
        <form action="" id="register-form">
        <input type="text" name="login" placeholder="Ваш логин" required>
        <input type="email" name="email" placeholder="Ваш email" required>
        <input type="password" name="password" placeholder="Ваш пароль" required>
        <input type="password" name="repeat-password" placeholder="Повторите пароль" required>
        <input type="submit" value="Регистрация">
        </form>
        </div>
        </div>

2.  Всплывающее окно с формой авторизации. Его HTML-разметка:

        <div id="idОкна" class="классыОкна">
        <div class="modal-content">
        <span class="close">&times;</span>
        <form action="" id="login-form">
        <input type="text" name="login" placeholder="Ваш логин или email" required>
        <input type="password" name="password" placeholder="Ваш пароль" required>
        <input type="submit" value="Вход">
        </form>
        </div>
        </div>

Формы создавать отдельными функциями-конструкторами LoginForm, RegisterForm.

Привяжите открытие первого окна к кнопке Регистрация, а второго - к кнопке Вход

      <style>
      body {
      font-family: Arial, Helvetica, sans-serif;
      }

      /* The Modal (background) */
      .modal {
      display: none;
      /* Hidden by default */
      position: fixed;
      /* Stay in place */
      z-index: 1;
      /* Sit on top */
      padding-top: 100px;
      /* Location of the box */
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      width: 200px;
      /* Full width */
      /*height: 100px;*/
      /* Full height */
      overflow: auto;
      /* Enable scroll if needed */
      background-color: rgb(0, 0, 0);
      /* Fallback color */
      background-color: rgba(0, 0, 0, 0.4);
      /* Black w/ opacity */
      background-color: #fefefe;
      padding: 20px;
      border: 1px solid #888;

      }

      .modal.active {
      display: block;
      }

      /* The Close Button */
      .close {
      color: #aaaaaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
      }

      .close:hover,
      .close:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
      }


      </style>
