// const obj = {}

// Object.create()

// function User(name, age) {
//     this.name = name;
//     this.age = age;
// }

// const user = new User("Uasya", 20);
// const user2 = new User("Uasya", 20);

// console.log(user);
// console.log(user2);
// console.log(user.__proto__);

// console.log(User.prototype);

// console.log(user.__proto__ === User.prototype);

// User.prototype.showName = function () {
//     return this.name;
// };

// console.log(user.showName());

// let bar = { a: "a4" };
// function foo() {}
// foo.prototype = bar;

// const baz = Object.create(bar);

// console.log(baz instanceof foo);
// console.log(baz.__proto__ === foo.prototype);

// CLASSES

// class User {
//     // before 2022

//     constructor(name, age) {
//         this.name = name;
//         this.age = age;
//     }
//     status = 'active';

//     static changeStatus(value) {
//         this.status = value;
//     }
//     // after 2022
//     // name = "Uasya";
//     // age = 20;
//     // _secondName = "Petrovich";
//     #secondName = 'Petrovich';

//     showName() {
//         return this.name;
//     }
//     showSecondName() {
//         return this.#secondName;
//     }
//     changeSecondName(value) {
//         this.#secondName = value;
//     }
// }

// // User.changeStatus("offline");
// // console.log(User);
// const user = new User('Uasya', 20);
// const user = new User();
// console.log(user);

// // console.log(user.#secondName);
// console.log(user.showName());
// user.changeSecondName("Gadya");
// console.log(user.showSecondName());

// Object.create({});
// console.log(Object.hasOwn(user, "status"));

// class Admin extends User {
//     constructor(name, age, adminLevel) {
//         super(name, age);
//         this.adminLevel = adminLevel;
//     }

//     shoSN(value) {
//         super.changeSecondName(value);
//     }
//     showName() {
//         return super.showName() + ' greatest!';
//     }
// }

// const admin = new Admin('Uasya', 20, 'superAdmin');
// console.log(admin);
// console.log(admin.showName());
// // admin.shoSN("PPd");
// // console.log(admin);
// admin.changeSecondName('Gadya');
// console.log(admin);

// task 1
// class Patient {
//     constructor(fullName, dataOfBirth, sex) {
//         this.fullName = fullName;
//         this.dataOfBirth = dataOfBirth;
//         this.sex = sex;
//     }
// }
// class CardiologistP extends Patient {
//     constructor(fullName, dataOfBirth, sex, pressure, healthProblems) {
//         super(fullName, dataOfBirth, sex);
//         this.pressure = pressure;
//         this.healthProblems = healthProblems;
//     }
// }
// const patient = new Patient('Petya V', 1993, 'male');
// const cardiologistP = new CardiologistP('Petya V', 1993, 'male', 50, 'good');

// console.log(patient);
// console.log(cardiologistP);

// class DentistP extends Patient {
//     constructor(fullName, dataOfBirth, sex, lastVisits, currentTreatment) {
//         super(fullName, dataOfBirth, sex);
//         this.lastVisits = lastVisits;
//         this.currentTreatment = currentTreatment;
//     }
// }
// const dentistP = new DentistP('Petya V', 1993, 'male', 'last week', 'coronka');
// console.log(dentistP);

// task 2

// class Modal {
//     constructor(id, classTest, text) {
//         this.id = id;
//         this.classTest = classTest;
//         this.text = text;
//     }
//     render(root) {
//         this.modalWindow = document.createElement('div');
//         this.modalWindow.id = this.id;
//         this.modalWindow.classList.add(...this.classTest);
//         this.modalWindow.innerHTML = `<div class="modal-content">
//         <span class="close">&times;</span>
//         <p>${this.text}</p>
//         </div>`;
//         root.append(this.modalWindow);
//         const btnClose = document.querySelector('.close');

//         btnClose.addEventListener('click', modal.closeModal);
//     }
//     openModal = () => {
//         this.modalWindow.classList.add('active');
//     };

//     closeModal = () => {
//         this.modalWindow.classList.remove('active');
//     };
// }

// const modal = new Modal(1, ['modal', 'q', 'w'], 'qwewqe');
// modal.render(document.querySelector('#root'));

// const btn = document.querySelector('#myBtn');
// btn.addEventListener('click', modal.openModal);

// task 3

class Modal {
    constructor(id, classList) {
        this.id = id;
        this.classList = classList;
    }
    render(root) {
        this.modalContainer = document.createElement('div');
        this.modalContent = document.createElement('div');
        const modalClose = document.createElement('span');

        this.modalContainer.id = this.id;
        this.modalContainer.classList.add(...this.classList);

        this.modalContent.className = 'modal-content';

        modalClose.className = 'close';
        modalClose.innerHTML = '&times;';
        modalClose.addEventListener('click', this.closeModal);

        this.modalContent.append(modalClose);
        this.modalContainer.append(this.modalContent);
        root.append(this.modalContainer);
    }
    openModal = () => {
        this.modalContainer.classList.add('active');
    };

    closeModal = () => {
        this.modalContainer.classList.remove('active');
    };
}



class Register extends Modal {
    constructor(id, classTest) {
        super(id, classTest);
    }
    render(root) {
        super.render(root);
        const registerForm = document.createElement('form');
        registerForm.action = '#';
        registerForm.id = ' register-form';
        registerForm.innerHTML = `<input type="text" name="login" placeholder="Ваш логин" required>
        <input type="email" name="email" placeholder="Ваш email" required>
        <input type="password" name="password" placeholder="Ваш пароль" required>
        <input type="password" name="repeat-password" placeholder="Повторите пароль" required>
        <input type="submit" value="Регистрация">`;
        this.modalContent.append(registerForm);
    }
}
class Authorization extends Modal {
    constructor(id, classTest) {
        super(id, classTest);
    }
    render(root) {
        super.render(root);


        const autoForm = document.createElement('form');
        autoForm.action = '#';
        autoForm.id = ' register-form1';
        autoForm.innerHTML = `<input type="text" name="login" placeholder="Ваш логин или email" required>
        <input type="password" name="password" placeholder="Ваш пароль" required>
        <input type="submit" value="Вход">`;
        this.modalContent.append(autoForm);
    }
}



const register = new Register('modal', ['modal']);
const authorization = new Authorization('modal11', ['modal']);

const btnRegister = document.querySelector('#btnRegister');
const btn3 = document.querySelector('#btn3');
const root = document.querySelector('#root');

register.render(root);
btnRegister.addEventListener(
    'click',

    register.openModal,
    //register.render(root)
);

authorization.render(root);
btn3.addEventListener('click', authorization.openModal);

