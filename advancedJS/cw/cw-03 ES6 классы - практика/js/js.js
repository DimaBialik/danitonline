class Input {
    constructor(
        type,
        name,
        required,
        id,
        classes,
        placeholder,
        errorText,
        value
    ) {
        this.type = type;
        this.name = name;
        this.required = required;
        this.id = id;
        this.classes = classes;
        this.placeholder = placeholder;
        this.errorText = errorText;
        this.value = value;
    }
    render() {
        this.input = document.createElement('input');
        this.input.setAttribute('type', this.type);
        this.input.setAttribute('name', this.name);
        this.input.setAttribute('required', this.required);
        this.input.setAttribute('id', this.id);
        this.input.classList.add(...this.classes);
        this.input.setAttribute('placeholder', this.placeholder);

        if (this.input.type === 'submit') {
            this.input.setAttribute('value', 'submit');
        }

        if (this.input.type !== 'submit') {
            this.input.addEventListener('blur', this.handlerBlur.bind(this));
            this.p = document.createElement('p');
            this.p.innerText = this.errorText;
            this.p.style.display = 'none';
        }

        return this.input;
    }

    handlerBlur(e) {
        if (e.target.value.trim() === '') {
            this.p.style.display = 'block';
            this.input.style.background = 'red';
        } else {
            this.p.style.display = 'none';
            this.input.style.background = 'none';
        }
        this.input.after(this.p);
    }
}

class Modal {
    constructor(id, classList) {
        this.id = id;
        this.classList = classList;
    }
    render(root) {
        this.modalContainer = document.createElement('div');
        this.modalContent = document.createElement('div');
        const modalClose = document.createElement('span');

        this.modalContainer.id = this.id;
        this.modalContainer.classList.add(...this.classList);

        this.modalContent.className = 'modal-content';

        modalClose.className = 'close';
        modalClose.innerHTML = '&times;';
        modalClose.addEventListener('click', this.closeModal);

        this.modalContent.append(modalClose);
        this.modalContainer.append(this.modalContent);
        root.append(this.modalContainer);
    }
    openModal = () => {
        this.modalContainer.classList.add('active');
    };

    closeModal = () => {
        this.modalContainer.classList.remove('active');
    };
}

class Register extends Modal {
    constructor(id, classTest) {
        super(id, classTest);
    }
    render(root) {
        super.render(root);
        const registerForm = document.createElement('form');

        const inputLogin = new Input(
            'text',
            'login',
            'required',
            ' registerIdInputLogin',
            ['classesInputLogin', 'classesInputLogin1'],
            'Ваш логин или email',
            'wtf'
        ).render();
        const inputEmail = new Input(
            'email',
            'email',
            'required',
            ' registerIdInputEmail',
            ['classesInputEmail'],
            'Ваш  email',
            'wtf'
        ).render();
        const inputPassword = new Input(
            'password',
            'password',
            'required',
            ' registerIdInputPassword',
            ['classesInputPassword'],
            'password',
            'wtf'
        ).render();
        const inputRePassword = new Input(
            'password',
            'repeat-password',
            'required',
            ' registerIdInputRePassword',
            ['classesInputPassword'],
            'repassword',
            'wtf'
        ).render();
        const inputSubmit = new Input(
            'submit',
            'submit',
            'required',
            ' registerIdInputSubmit',
            ['classesInputSubmit'],
            'submit',
            'wtf'
        ).render();
        this.modalContent.append(registerForm);
        registerForm.append(
            inputLogin,
            inputEmail,
            inputPassword,
            inputRePassword,
            inputSubmit
        );
    }
}
class Authorization extends Modal {
    constructor(id, classTest) {
        super(id, classTest);
    }
    render(root) {
        super.render(root);
        const autoForm = document.createElement('form');

        autoForm.id = 'login-form';
        autoForm.action = '#';

        const inputLogin = new Input(
            'text',
            'login',
            'required',
            'idInputLogin',
            ['classesInputLogin', 'classesInputLogin1'],
            'Ваш логин или email',
            'wtf'
        ).render();

        const inputPassword = new Input(
            'password',
            'password',
            'required',
            'idInputPassword',
            ['classesInputPassword'],
            'password',
            'wtf'
        ).render();
        const inputSubmit = new Input(
            'submit',
            'submit',
            'required',
            ['idInputSubmit'],
            'classesInputSubmit',
            'submit',
            'wtf'
        ).render();
        this.modalContent.append(autoForm);
        autoForm.append(inputLogin, inputPassword, inputSubmit);
    }
}

const register = new Register('modal', ['modal']);
const authorization = new Authorization('modal11', ['modal']);

const btnRegister = document.querySelector('#btnRegister');
const btn3 = document.querySelector('#btn3');
const root = document.querySelector('#root');

register.render(root);
btnRegister.addEventListener(
    'click',

    register.openModal
);

authorization.render(root);
btn3.addEventListener('click', authorization.openModal);
