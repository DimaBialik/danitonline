//task 1
// class Patient {
//     constructor([height, weight]) {
//         this.height = height / 100;
//         this.weight = weight;
//     }
//     getPatientStatus() {
//         const index = Math.round(this.weight / this.height ** 2);
//         let degree = '';

//         if (index <= 15) {
//             degree = 'анорексия';
//         } else if (index >= 16 && index <= 25) {
//             degree = 'норма';
//         } else if (index >= 26 && index <= 30) {
//             degree = 'лишний вес;';
//         } else if (index >= 31 && index <= 35) {
//             degree = 'I степень';
//         } else if (index >= 36 && index <= 40) {
//             degree = 'II степень';
//         } else if (index >= 41) {
//             degree = 'III степень';
//         }
//         return [index, degree];
//     }
// }

// const patient = new Patient([178, 76]);
// patient.getPatientStatus();
// console.log(patient.getPatientStatus());

//task 2

class User {
    constructor(fName, lName, ...rest) {
        this.fName = fName;
        this.lName = lName;
        rest.forEach(el => {
            let [nameOfEl, valueOfEl] = el.split(':');
            this[nameOfEl.trim()] = valueOfEl.trim();
        });
    }
}
const user = new User(
    'Золар',
    'Аперкаль',
    'status: глава Юногоклана Мескии',
    'wife: Иврейн'
);

console.log(user);

// task 3

class Warrior {
    constructor({ name, status, weapon }) {
        this.name = name;
        this.status = status;
        this.weapon = weapon;
    }
}

const warrior = new Warrior({
    name: 'Dima',
    lastName: 'Bialik',
    weapon: 'eagle',
    status: 'open',
    vid: 'value'
});
console.log(warrior);
// task 4

const resume = {
    name: 'Илья',
    lastName: 'Куликов',
    age: 29,
    city: 'Киев',
    skills: [
        { name: 'Vanilla JS', practice: 5 },
        { name: 'ES6', practice: 3 },
        { name: 'React + Redux', practice: 1 },
        { name: 'HTML4', practice: 6 },
        { name: 'CSS2', practice: 6 }
    ]
};

const vacancy = {
    company: 'SoftServe',
    location: 'Киев',
    skills: [
        { name: 'Vanilla JS', experience: 3 },
        { name: 'ES6', experience: 2 },
        { name: 'React +Redux', experience: 2 },
        { name: 'HTML4', experience: 2 },
        { name: 'CSS2', experience: 2 },
        { name: 'HTML5', experience: 2 },
        { name: 'CSS3', experience: 2 },
        { name: 'AJAX', experience: 2 },
        { name: 'Webpack', experience: 2 }
    ]
};

class ApplyResume {
    constructor() { }
}

const applyResume = new ApplyResume(resume, vacancy);
