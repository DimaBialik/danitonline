//task 1
// const url = 'https://ajax.test-danit.com/api/swapi/planets';
// const root = document.querySelector('#root');
// const promptPlanets = +prompt('u planet');
// const promptPlanetsTwo = +prompt('u planet');

// class Planets {
//     constructor(url, root) {
//         this.url = url;
//         this.root = root;
//     }
//     request() {
//         return fetch(this.url, {
//             method: 'GET',
//             headers: {
//                 'Content-Type': 'application/json'
//             }
//         })
//             .then(response => {
//                 return response.json();
//             })
//             .catch(e => {
//                 console.log(e);
//             });
//     }
//     render(promptPlanets) {
//         this.request().then(data => {
//             const filtered = data.find(el => el.id === promptPlanets);

//             let {
//                 climate,
//                 diameter,
//                 gravity,
//                 name,
//                 orbitalPeriod,
//                 population
//             } = filtered;
//             const divPlanets = document.createElement('div');
//             divPlanets.innerHTML = `<p>name: ${name}</p>
//                 <p>climate: ${climate}</p>
//                 <p>diameter: ${diameter}</p><p>gravity: ${gravity}</p>
//                 <p>orbitalPeriod: ${orbitalPeriod}</p>
//                 <p>population: ${population}</p>`;
//             this.root.append(divPlanets);
//         });
//     }
// }
// const planets = new Planets(url, root);
// planets.render(promptPlanets);
// planets.render(promptPlanetsTwo);

//task 2

const URI = 'https://ajax.test-danit.com/api/swapi/';
const root = document.querySelector('#root');

class Heroes {
    constructor(url, root) {
        this.url = url;
        this.root = root;
    }

    request(entity, id = '') {
        return fetch(`${this.url}${entity}/${id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(response);
                } else {
                    return response.json();
                }
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        this.request('people').then(data => {
            const select = document.createElement('select');
            const options = data.map(({ name, homeworld }) => {
                const option = document.createElement('option');
                option.value = homeworld;
                option.textContent = name;
                return option;
            });
            select.append(...options);
            this.root.append(select);
            select.addEventListener('change', e => {
                const planetId = e.target.value.split(`${this.url}planets/`)[1];
                this.request('planets', planetId).then(data => {
                    this.renderPlanetInfo(data);
                });
            });
        });
    }

    renderPlanetInfo({
        climate,
        diameter,
        gravity,
        name,
        orbitalPeriod,
        population
    }) {
        const divPlanets = document.createElement('div');
        divPlanets.innerHTML = `<p>name: ${name}</p>
                <p>climate: ${climate}</p>
                <p>diameter: ${diameter}</p><p>gravity: ${gravity}</p>
                <p>orbitalPeriod: ${orbitalPeriod}</p>
                <p>population: ${population}</p>`;
        this.root.append(divPlanets);
    }
}

new Heroes(URI, root).render();
