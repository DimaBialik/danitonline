class ChangeTextSize {
    constructor(sizeText) {
        this.sizeText = sizeText;
        this.sizeBefore = parseFloat(this.sizeText.style.fontSize);
    }

    increase(size) {
        this.sizeBefore += size;
        this.sizeText.style.fontSize = this.sizeBefore + 'px';
    }
    decrease(size) {
        this.sizeBefore -= size;
        this.sizeText.style.fontSize = this.sizeBefore + 'px';
    }
}
const text = document.querySelector('#text');

const increaseBtn = document.querySelector('#increase-text');
const decreaseBtn = document.querySelector('#decrease-text');
const changeTextSize = new ChangeTextSize(text);

increaseBtn.addEventListener('click', () => {
    changeTextSize.increase(2);
});

decreaseBtn.addEventListener('click', () => {
    changeTextSize.decrease(3);
});

class CreateProduct {
    constructor(name, fullName, article, price) {
        this.name = name;
        this.fullName = fullName;
        this.article = article;
        this.price = price;
    }
}
//const createProduct = new CreateProduct();

const notebook = new CreateProduct(
    'lenovo X120S',
    'lenovo X120S(432 - 44) W',
    3332,
    23244
);
console.log(notebook.price); // выведет 23244
notebook.price = -4;
console.log(notebook.price);
notebook.price = 22000;
console.log(notebook.price);

class Slider {
    constructor(effect, speed, autostart, slidesList) {
        this.effect = effect;
        this.speed = speed;
        this.autostart = autostart;
        this.slidesList = slidesList;
    }
    addSlide(slide) {
        try {
            if (slide instanceof Object) {
                this.slidesList.push(slide);
            }
        } catch (e) {
            console.log(e);
        }
    }
    delSlide(id) {
        this.slidesList = this.slidesList.filter(value => {
            return value.id !== id;
        });
    }
    gedSlide(id) {
        return this.slidesList.find(value => value.id === id);
    }
}
const slider = new Slider('slow', 15, true, [
    { id: 'slideOne' },
    { id: 'slideTwo' }
]);
slider.addSlide({ id: 'slideThee' });
slider.delSlide('slideOne');
slider.gedSlide('slideTwo');
console.log(slider);

class Validation {
    checkEmail(item) {
        if (item) {
        }
    }
    checkPhone() { }
    checkPassport() { }
}
const validation = new Validation();

const email = document.querySelector('#email');
email.addEventListener('keypress', e => {
    validation.checkEmail(e.key);
});
// const phone = document.querySelector('#phone');
// phone.addEventListener('keypress');
// const passport = document.querySelector('#passport');
// passport.addEventListener('keypress');
