// const BASE_URI = 'http://localhost:3000';

// // const resp = await fetch(BASE_URI + 'posts');
// // const data = await resp.json();

// // console.log(data);

// const getPost = async () => {
//     const resp = await fetch(BASE_URI + '/posts');
//     const data = await resp.json();
//     return data;
// };
// getPost().then(data => {
//     console.log(data);
// });
// console.log('done');

// TASKS

const url = 'http://localhost:3000';
const root = document.querySelector('#root');

class Request {
    constructor(url) {
        this.url = url;
    }

    post(json) {
        return fetch(`${this.url}/posts`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: json
        });
    }
}

const request = new Request(url);

class Post {
    renderList(root) {
        this.postsList = document.querySelector('#posts-list').content;
        this.list = this.postsList.querySelector('ul').cloneNode();
        root.append(this.list);
    }

    renderItem(data) {
        const listItem = this.postsList.querySelector('li').cloneNode();
        console.log(this.postsList);
        const { title, intro, text, createAt, author } = data;
        listItem.innerHTML = `<p>${title}</p>
                        <p>${intro}</p>
                        <p>${text}</p>
                        <p>${createAt}</p>
                        <p>${author}</p>
<button type="button">[Edit]</button>`;
        this.list.append(listItem);
    }
}

const post = new Post();
post.renderList(root);

class Form {
    constructor(root) {
        this.root = root;
    }

    render() {
        const formFragment = document.querySelector('#create-post').content;
        this.form = formFragment.querySelector('form');
        this.root.prepend(formFragment);
        this.form.addEventListener('submit', e => {
            e.preventDefault();
            const formsElements = this.formCheck(e.target);
            if (formsElements) {
                const objForm = {};
                formsElements.forEach(({ id, value }) => {
                    objForm[id] = value;
                });

                const jsonForm = JSON.stringify(objForm);
                this.publishPost(jsonForm);
            } else {
                alert('Please enter all fields!');
            }
        });
    }

    formCheck(form) {
        const formElements = [...form.elements].filter(
            ({ type }) => type !== 'submit'
        );
        const formElementsNotEmpty = formElements.filter(el => {
            if (el.value.trim()) {
                return el;
            }
        });
        return formElements.length === formElementsNotEmpty.length
            ? formElementsNotEmpty
            : false;
    }

    publishPost(jsonData) {
        request
            .post(jsonData)
            .then(response => response.json())
            .then(data => {
                this.form.reset();
                post.renderItem(data);
            });
    }
}

new Form(root).render();
