class Card {
    constructor(id, userId, title, body, name, email) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
        this.name = name;
        this.email = email;
    }
    createElement() {
        this.wrapper = document.createElement('div');
        this.wrapper.classList = 'wrapper';
        this.delBtn = document.createElement('button');
        this.delBtn.innerText = 'DELETE';

        this.delBtn.addEventListener('click', () => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                method: 'DELETE'
            })
                .then(res => {
                    if (res.ok) {
                        this.wrapper.remove();
                    } else {
                        throw new Error();
                    }
                })
                .catch(err => console.log(err.message));
        });
        this.wrapper.innerHTML = `
        <p>post# ${this.id}</p>
        <div class="card">
                <p>${this.name}</p>
        <p>${this.email}</p>
        </div>
        <p>${this.title}</p>
        <p>${this.body}</p>
        `;
        this.wrapper.append(this.delBtn);
    }
    render(root) {
        this.createElement();
        return root.append(this.wrapper);
    }
}

Promise.all([
    fetch('https://ajax.test-danit.com/api/json/users').then(res =>
        res.json().then(data => data)
    ),
    fetch('https://ajax.test-danit.com/api/json/posts').then(res =>
        res.json().then(data => data)
    )
])
    .then(data => {
        const [userArr, postsArr] = data;
        let cardArr = document.createElement('div');

        postsArr.forEach(({ id, userId, title, body }) => {
            const user = userArr.find(item => item.id === userId);
            new Card(id, userId, title, body, user.name, user.email).render(
                cardArr
            );
        });
        const root = document.querySelector('#root');
        root.append(cardArr);
    })
    .catch(err => console.log(err.message));
