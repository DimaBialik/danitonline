const root = document.querySelector('#root');
const BASE_URL = 'https://ajax.test-danit.com/api/swapi/films';

class Films {
    constructor(url) {
        this.url = url;
     
    }
    render(root) {
        this.request(this.url).then(data=>{
           
            const filmsInfo = data.map(
                    ({ episodeId, name, openingCrawl, characters }) => {
                        const fragment = document.createDocumentFragment();
                        const divInfo = document.createElement('div');
                        const episodeIdText = document.createElement('p');
                        const nameText = document.createElement('p');
                        const openingCrawlText = document.createElement('p');
                        episodeIdText.innerText = ` епізод: ${episodeId} `;
                        nameText.innerText = ` назва: ${name} `;
                        openingCrawlText.innerText = `опис: ${openingCrawl} `;
                        divInfo.append(
                            episodeIdText,
                            nameText,
                            openingCrawlText
                        );
                        fragment.append(divInfo);
                        characters.forEach( el=> {
                           this.request(el)  .then( ({name})=> {
                              const filmActors = document.createElement('p');
                              filmActors.innerText = name;
                              divInfo.after(filmActors)
                             })
                                                    
                        })
                        return fragment;
                    }
            );
               root.append(...filmsInfo)
        }) .catch(e => {
            console.log(e);
        });
            
    }
    request(url) {
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(response);
                } else {
                 return    response.json();
                    
                }
            })
            
    }
 
}
const films = new Films(BASE_URL);
films.render(root);


