const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  class BookRender{
      constructor(books){
          this.books=books

      }
      render(){ 
          const div = document.querySelector('#root')

          this.books.forEach(item => {
            const listChild = document.createElement('ul');
            const author = document.createElement('li');
            const name = document.createElement('li');
            const price = document.createElement('li');
    
            listChild.innerHTML = `Книга `;
            author.innerHTML = `Имя автора: ${item.author}`;
            name.innerHTML = `Название: ${item.name}`;
            price.innerHTML = `Цена: ${item.price}`;
                
            try {
                if (!item.name) {
                    throw new SyntaxError(` Свойства name нет в книге автора ${item.author}`);
                }
                if (!item.author) {
                    throw new SyntaxError(` Свойства author нет в книге с названием ${item.name}`);
                }
                if (!item.price) {
                    throw new SyntaxError(` Свойства price нет в книге с названием ${item.name}`);
                }
            } catch (error) {
                console.log('Ошибка входных данных.' + error.message);
            }finally{
                if (item.author !== undefined && item.name !== undefined && item.price !== undefined) {
                    div.append(listChild);
                    listChild.append(author, name, price);
                }
            }
        });

      }
  }
  const bookRender = new BookRender(books)
  bookRender.render()

