const btn = document.createElement('button');

class ShowIp {
    constructor() {
        this.giveIp = 'https://api.ipify.org/?format=json';
        this.getIp = 'http://ip-api.com/json/';
        this.root = document.querySelector('#root');
        this.textIp = document.createElement('p');
        this.btn = document.createElement('button');
    }
    async request() {
        const { ip } = await fetch(this.giveIp).then(res => res.json());

        const data = await fetch(`${this.getIp}${ip}`).then(res => res.json());
        return data;
    }
    handleIp() {
        this.request().then(({ country, city, timezone, query }) => {
            this.textIp.innerHTML = `
            <p>IP: ${query}</p>
             <p>country: ${country}</p>
              <p>city: ${city}</p>
              <p>timezone: ${timezone}</p>   `;
            this.root.append(this.textIp);
        });
    }
    clickBtn() {
        root.before(btn);
        btn.innerText = 'click me';
        btn.addEventListener('click', () => {
            this.handleIp();
        });
    }
}
new ShowIp().clickBtn();
