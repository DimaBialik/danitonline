import {
    VisitCardsDentist,
    VisitCardsCardiologist,
    VisitCardsTherapist
} from '../classes/renderVisitCards.js';
import visitsArray from '../index.js';
async function getRequest() {
    await fetch('https://ajax.test-danit.com/api/v2/cards', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `${localStorage.getItem('Authorization')}`
        }
    })
        .then(response => response.json())
        .then(data => {
            visitsArray.length = 0;
            console.log(data);
            data.forEach(element => {
                if (element.doctor === 'Dentist') {
                    const {
                        name,
                        doctor,
                        description,
                        urgency,
                        purpose,
                        status,
                        id,
                        lastVisit
                    } = element;
                    const newElement = new VisitCardsDentist(
                        name,
                        doctor,
                        description,
                        urgency,
                        purpose,
                        status,
                        id,
                        'none',
                        'show more',
                        lastVisit
                    );

                    newElement.render();

                    visitsArray.push(newElement);

                    if (visitsArray.length > 0) {
                        const err = document
                            .querySelector('.empty-card')
                            ?.remove();
                    }
                } else if (element.doctor === 'Cardiologist') {
                    const {
                        name,
                        doctor,
                        description,
                        urgency,
                        purpose,
                        status,
                        id,
                        ageCardiologist: age,
                        pressure,
                        massIndex,
                        diseases
                    } = element;
                    const newElement = new VisitCardsCardiologist(
                        name,
                        doctor,
                        description,
                        urgency,
                        purpose,
                        status,
                        id,
                        'none',
                        'show more',
                        age,
                        pressure,
                        massIndex,
                        diseases
                    );

                    newElement.render();
                    visitsArray.push(newElement);

                    if (visitsArray.length > 0) {
                        const err = document
                            .querySelector('.empty-card')
                            ?.remove();
                    }
                } else if (element.doctor === 'Therapist') {
                    const {
                        name,
                        doctor,
                        description,
                        urgency,
                        purpose,
                        status,
                        id,
                        ageTherapist: age
                    } = element;
                    const newElement = new VisitCardsTherapist(
                        name,
                        doctor,
                        description,
                        urgency,
                        purpose,
                        status,
                        id,
                        'none',
                        'show more',
                        age
                    );

                    newElement.render();
                    visitsArray.push(newElement);

                    if (visitsArray.length > 0) {
                        const err = document
                            .querySelector('.empty-card')
                            ?.remove();
                    }
                }
            });
        });
}

export default getRequest;
