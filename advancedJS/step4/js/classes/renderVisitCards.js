import visitsArray from '../index.js';
import deleteCards from '../api/deleteCards.js';

import { CardiologistEdit, TherapistEdit, DentistEdit } from './modal-edit.js';

class VisitCards {
    constructor(
        name,
        doctor,
        description,
        urgency,
        purpose,
        status,
        id,
        display,
        showMoreBtnText
    ) {
        this.name = name;
        this.doctor = doctor;
        this.description = description;
        this.priority = urgency;
        this.visitPurpouse = purpose;
        this.status = status;
        this.cardWrapper = document.createElement('div');
        this.cardWrapper.classList.add('card__unit');
        this.moreInfo = document.createElement('div');
        this.id = id;
        this.display = display;
        this.showMoreBtnText = showMoreBtnText;
    }

    createElements() {
        this.cardWrapper.classList.add(`card-id__${this.id}`);
        this.moreInfo.style.display = `${this.display}`;
        this.showMoreBtn = document.createElement('button');
        this.deleteCards = document.createElement('button');
        this.deleteCards.innerText = 'DELET';
        this.showMoreBtn.innerText = this.showMoreBtnText;
        this.editBtn = document.createElement('button');
        this.DeleteBtn = document.createElement('button');
        this.editBtn.innerText = 'edit';
        this.editBtn.addEventListener('click', () => {
            console.log(this.doctor);
            if (this.doctor === 'Dentist') {
                new DentistEdit(this).render();
            } else if (this.doctor === 'Therapist') {
                new TherapistEdit(this).render();
            } else if (this.doctor === 'Cardiologist') {
                new CardiologistEdit(this).render();
            } else {
                console.log('UNKNOWN DOCTOR');
            }
        });
        this.moreInfo.insertAdjacentHTML(
            'afterbegin',
            `
        <p><span>DESCRIPTION</span>: ${this.description}</p>
        <p><span>VISIT PURPOUSE</span>:${this.visitPurpouse}</p>
        `
        );
        //  console.log(visitsArray);
        this.deleteCards.addEventListener('click', e => {
            visitsArray.length--;
            e.preventDefault();
            deleteCards(this.id);
            this.cardWrapper.remove();
            console.log(visitsArray);
            console.log(visitsArray.length);
            if (visitsArray.length === 0) {
                document.querySelector(
                    '.card_container'
                ).innerHTML = `<h2 class="empty-card">the visits is empty</h2>`;
            } else {
                const err = document.querySelector('.empty-card')?.remove();
            }
        });
        this.showMoreBtn.addEventListener('click', e => {
            if (this.display === 'none') {
                this.moreInfo.style.display = 'block';
                this.showMoreBtn.innerText = 'show less';
                this.display = 'block';
                visitsArray.find(el => {
                    if (el.id === this.id) {
                        el.display = 'block';
                        el.showMoreBtn.innerText = 'show less';
                    }
                });
            } else {
                this.moreInfo.style.display = 'none';
                this.showMoreBtn.innerText = 'show more';
                this.display = 'none';
                visitsArray.find(el => {
                    if (el.id === this.id) {
                        el.display = 'none';
                        el.showMoreBtn.innerText = 'show more';
                    }
                });
            }
        });

        this.cardWrapper.insertAdjacentHTML(
            'beforeend',
            `
            <h2><span>NAME</span>: ${this.name}</h2>
            <h3><span>DOCTOR</span>: ${this.doctor}</h3>
            <p><span>PRIORITY</span>: ${this.priority}</p>
            <p><span>STATUS</span>: ${this.status}</p>
            `
        );
        this.cardWrapper.append(
            this.deleteCards,
            this.moreInfo,
            this.showMoreBtn,
            this.editBtn
        );
    }
    render(selector = document.querySelector('.card_container')) {
        this.createElements();
        selector.append(this.cardWrapper);
    }
}

//export default VisitCards;
export class VisitCardsCardiologist extends VisitCards {
    constructor(
        name,
        doctor,
        description,
        urgency,
        purpose,
        status,
        id,
        display = 'none',
        showMoreBtnText = 'show more',
        age,
        pressure,
        massIndex,
        diseases
    ) {
        super(
            name,
            doctor,
            description,
            urgency,
            purpose,
            status,
            id,
            display,
            showMoreBtnText
        );
        this.age = age;
        this.pressure = pressure;
        this.massIndex = massIndex;
        this.diseases = diseases;
    }
    createElements() {
        super.createElements();
        this.moreInfo.insertAdjacentHTML(
            'afterbegin',
            `
      <p>AGE: ${this.age}</p>
      <p>PREASSURE: ${this.pressure}</p>
      <p>MASS INDEX: ${this.massIndex}</p>
      <p>DISEASES: ${this.diseases}</p>

      `
        );
    }
    render() {
        super.render();
    }
}
export class VisitCardsDentist extends VisitCards {
    constructor(
        name,
        doctor,
        description,
        urgency,
        purpose,
        status,
        id,
        display = 'none',
        showMoreBtnText = 'show more',
        lastVisit
    ) {
        super(
            name,
            doctor,
            description,
            urgency,
            purpose,
            status,
            id,
            display,
            showMoreBtnText
        );
        this.lastVisit = lastVisit;
    }
    createElements() {
        super.createElements();
        this.moreInfo.insertAdjacentHTML(
            'afterbegin',
            `
    <p>LAST VISIT DATE: ${this.lastVisit}</p>
    `
        );
    }
    render() {
        super.render();
    }
}
export class VisitCardsTherapist extends VisitCards {
    constructor(
        name,
        doctor,
        description,
        urgency,
        purpose,
        status,
        id,
        display = 'none',
        showMoreBtnText = 'show more',
        age
    ) {
        super(
            name,
            doctor,
            description,
            urgency,
            purpose,
            status,
            id,
            display,
            showMoreBtnText
        );
        this.age = age;
    }
    createElements() {
        super.createElements();
        this.moreInfo.insertAdjacentHTML(
            'afterbegin',
            `
    <p>AGE: ${this.age}</p>
    `
        );
    }
    render() {
        super.render();
    }
}
