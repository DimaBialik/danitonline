import checkAndFilterCard from './checkAndFilterCards.js';
let renderFilteredCards = array => {
    array.forEach(element => {
        const { doctor, display } = element;
        checkAndFilterCard(doctor, element, display);
    });
};
console.log(renderFilteredCards);
export default renderFilteredCards;
