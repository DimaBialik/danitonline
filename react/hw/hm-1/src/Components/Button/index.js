import React from 'react';
import './style.scss';

function Button({ backgroundColor, text, onClick }) {
    return (
        <>
            <button
                type="button"
                className={'buttons'}
                onClick={onClick}
                style={{ backgroundColor: backgroundColor }}
            >
                {text}
            </button>
        </>
    );
}
export default Button;
