import React from 'react';
import Button from './Components/Button';
import Modal from './Components/Modal';
import './App.scss';

class App extends React.Component {
    state = {
        firstModal: false,
        secondModal: false
    };

    handleFirstModal = () => {
        this.setState({ firstModal: true });
    };
    handleSecondModal = () => {
        this.setState({ secondModal: true });
    };
    closeModal = event => {
        this.setState({ firstModal: false });
        this.setState({ secondModal: false });
    };

    render() {
        return (
            <div className="App">
                <Button
                    text={'Open first modal'}
                    backgroundColor={' #cbb047'}
                    onClick={() => this.handleFirstModal()}
                />
                <Button
                    text={'Open second modal'}
                    backgroundColor={'#177bed'}
                    onClick={() => this.handleSecondModal()}
                />
                {this.state.firstModal ? (
                    <Modal
                        header={'First Modal'}
                        text={'this is the place for the first modal'}
                        closeButton={true}
                        actions={
                            <>
                                <Button
                                    backgroundColor={'rgba(108, 40, 40, 0.41)'}
                                    onClick={() => this.closeModal()}
                                    text={' Ok'}
                                />

                                <Button
                                    backgroundColor={' rgba(108, 40, 40, 0.41)'}
                                    onClick={() => this.closeModal()}
                                    text={'Cancel'}
                                />
                            </>
                        }
                        onClick={event => this.closeModal(event)}
                    />
                ) : (
                    ''
                )}
                {this.state.secondModal ? (
                    <Modal
                        header={'Second Modal'}
                        text={'this is the place for the second modal'}
                        closeButton={true}
                        actions={
                            <>
                                <Button
                                    backgroundColor={'rgb(163 171 219)'}
                                    onClick={() => this.closeModal()}
                                    text={' Ok'}
                                />

                                <Button
                                    backgroundColor={'rgb(83 87 153)'}
                                    onClick={() => this.closeModal()}
                                    text={'Cancel'}
                                />
                            </>
                        }
                        onClick={event =>
                            event.currentTarget === event.target &&
                            this.closeModal()
                        }
                    />
                ) : (
                    ''
                )}
            </div>
        );
    }
}
export default App;
