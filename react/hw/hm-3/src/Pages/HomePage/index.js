import Cards from '../../Components/Cards';
import PropTypes from 'prop-types';
export function HomePage({
    handleSubmitOk,
    handleOpenModal,
    handleBasketClick,
    arrCards,
    handleFavoritesClick,
    inFavorites,
    btnName,
    modalHeaderText,
    showBtn,
    disableBtn
}) {
    return (
        <Cards
            handleSubmitOk={handleSubmitOk}
            handleOpenModal={handleOpenModal}
            disableBtn={disableBtn}
            modalHeaderText={modalHeaderText}
            handleBasketClick={handleBasketClick}
            cards={arrCards}
            handleFavoritesClick={handleFavoritesClick}
            inFavorites={inFavorites}
            btnName={btnName}
            showBtn={showBtn}
        />
    );
}
HomePage.propTypes = {
    arrCards: PropTypes.array,
    handleFavoritesClick: PropTypes.func,
    handleBasketClick: PropTypes.func,
    inFavorites: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

    modalHeaderText: PropTypes.string,
    btnName: PropTypes.string
};

HomePage.defaultProps = {
    arrCards: [],
    handleFavoritesClick: () => {},
    handleBasketClick: () => {},
    inFavorites: 0,

    modalHeaderText: '',
    btnName: ''
};
