import Cards from '../../Components/Cards';
import PropTypes from 'prop-types';
export function FavoritesPage({
    handleOpenModal,
    handleSubmitOk,
    handleBasketClick,
    arrCards,
    handleFavoritesClick,
    inFavorites,
    btnName,
    modalHeaderText,
    showBtn,
    disableBtn
}) {
    if (arrCards.length === 0) {
        return (
            <div>
                <h2>Favorites is empty</h2>
            </div>
        );
    } else {
        return (
            <Cards
                handleOpenModal={handleOpenModal}
                handleSubmitOk={handleSubmitOk}
                disableBtn={disableBtn}
                modalHeaderText={modalHeaderText}
                handleBasketClick={handleBasketClick}
                cards={arrCards}
                handleFavoritesClick={handleFavoritesClick}
                inFavorites={inFavorites}
                btnName={btnName}
                showBtn={showBtn}
            />
        );
    }
}
FavoritesPage.propTypes = {
    arrCards: PropTypes.array,
    handleFavoritesClick: PropTypes.func,
    handleBasketClick: PropTypes.func,
    inFavorites: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

    modalHeaderText: PropTypes.string,
    btnName: PropTypes.string
};

FavoritesPage.defaultProps = {
    arrCards: [],
    handleFavoritesClick: () => {},
    handleBasketClick: () => {},
    inFavorites: 0,

    modalHeaderText: '',
    btnName: ''
};
