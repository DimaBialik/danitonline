import React, { useEffect, useState } from 'react';
import Header from './Components/Header';
import Button from './Components/Button';
import Modal from './Components/Modal';
import { Routes, Route } from 'react-router-dom';
import { HomePage } from './Pages/HomePage';
import { FavoritesPage } from './Pages/FavoritesPage';
import { BasketsPage } from './Pages/BasketsPage';
import './App.scss';

function App() {
    const [arrCards, setArrCards] = useState([]);
    const [arrFavoriteCards, setArrFavoriteCards] = useState([]);
    const [arrBasketCards, setArrBasketCards] = useState([]);
    const [inFavorites, setInFavorites] = useState(0);
    const [basketCounter, setBasketCounter] = useState(0);
    const [headerModalText, setHeaderModalText] = useState(false);
    const [item, setItem] = useState(0);
    const [openModal, setOpenModal] = useState(false);

    const handleOpenModal = el => {
        setHeaderModalText(el);
        setOpenModal(true);
    };

    const handleCloseModal = () => {
        setOpenModal(false);
    };
    const handleSubmitOk = item => {
        setItem(item);
    };
    const handleSubmit = () => {
        handleBasketClick(item);
        handleCloseModal();
    };

    const handleFavoritesClick = id => {
        const copiedArr = arrCards;
        const findElIndex = copiedArr.findIndex(({ EAN }) => EAN === id);
        const findElIndexByDelete = arrFavoriteCards.findIndex(
            ({ EAN }) => EAN === id
        );

        const findEl = copiedArr.find(({ EAN }) => EAN === id);

        if (copiedArr[findElIndex].inFavorites) {
            copiedArr[findElIndex].inFavorites = false;
            setArrFavoriteCards(() => {
                const nextFavoriteArr = [
                    ...arrFavoriteCards.slice(0, findElIndexByDelete),
                    ...arrFavoriteCards.slice(findElIndexByDelete + 1)
                ];
                localStorage.setItem(
                    'FavoriteArr',
                    JSON.stringify(nextFavoriteArr)
                );
                return nextFavoriteArr;
            });

            setInFavorites(() => {
                const nextFavoriteCounter = parseInt(inFavorites) - 1;
                localStorage.setItem(
                    'inFavorites',
                    parseInt(nextFavoriteCounter)
                );
                return nextFavoriteCounter;
            });
        } else {
            copiedArr[findElIndex].inFavorites = true;

            setArrFavoriteCards(() => {
                const nextFavoriteArr = [...arrFavoriteCards, findEl];
                localStorage.setItem(
                    'FavoriteArr',
                    JSON.stringify(nextFavoriteArr)
                );
                return nextFavoriteArr;
            });

            setInFavorites(() => {
                const nextFavoriteCounter = parseInt(inFavorites) + 1;

                localStorage.setItem(
                    'inFavorites',
                    parseInt(nextFavoriteCounter)
                );
                return nextFavoriteCounter;
            });
        }

        setArrCards([...copiedArr]);
        localStorage.setItem('arrCards', JSON.stringify(arrCards));
    };

    const handleBasketClick = id => {
        const copiedArr = arrCards;
        const findElIndex = copiedArr.findIndex(({ EAN }) => EAN === id);
        const findElIndexByDelete = arrBasketCards.findIndex(
            ({ EAN }) => EAN === id
        );
        const findEl = copiedArr.find(({ EAN }) => EAN === id);

        if (copiedArr[findElIndex].basketCounter) {
            copiedArr[findElIndex].basketCounter = false;
            setArrBasketCards(() => {
                const nextBasketArr = [
                    ...arrBasketCards.slice(0, findElIndexByDelete),
                    ...arrBasketCards.slice(findElIndexByDelete + 1)
                ];
                localStorage.setItem(
                    'BasketArr',
                    JSON.stringify(nextBasketArr)
                );
                return nextBasketArr;
            });
            setBasketCounter(() => {
                const nextBasketCounter = parseInt(basketCounter - 1);
                localStorage.setItem(
                    'basketCounter',
                    parseInt(nextBasketCounter)
                );
                return nextBasketCounter;
            });
        } else {
            copiedArr[findElIndex].basketCounter = true;

            setArrBasketCards(() => {
                const nextBasketArr = [...arrBasketCards, findEl];
                localStorage.setItem(
                    'BasketArr',
                    JSON.stringify(nextBasketArr)
                );
                return nextBasketArr;
            });

            setBasketCounter(() => {
                const nextBasketCounter = parseInt(basketCounter + 1);
                localStorage.setItem(
                    'basketCounter',
                    parseInt(nextBasketCounter)
                );
                return nextBasketCounter;
            });
        }

        setArrCards([...copiedArr]);

        localStorage.setItem('arrCards', JSON.stringify(arrCards));
    };
    useEffect(() => {
        if (localStorage.getItem('arrCards')) {
            const items = JSON.parse(localStorage.getItem('arrCards'));
            if (localStorage.getItem('FavoriteArr')) {
                const itemsFavorite = JSON.parse(
                    localStorage.getItem('FavoriteArr')
                );
                setArrFavoriteCards([...itemsFavorite]);
            }
            if (localStorage.getItem('BasketArr')) {
                const itemsBasket = JSON.parse(
                    localStorage.getItem('BasketArr')
                );
                setArrBasketCards([...itemsBasket]);
            }
            if (
                localStorage.getItem('basketCounter') &&
                localStorage.getItem('inFavorites')
            ) {
                const basketCounter = localStorage.getItem('basketCounter');
                const inFavorites = localStorage.getItem('inFavorites');

                setInFavorites(parseInt(inFavorites));
                setBasketCounter(parseInt(basketCounter));
            } else if (localStorage.getItem('inFavorites')) {
                const inFavorites = localStorage.getItem('inFavorites');
                setInFavorites(parseInt(inFavorites));
            } else if (localStorage.getItem('basketCounter')) {
                const basketCounter = localStorage.getItem('basketCounter');
                setBasketCounter(parseInt(basketCounter));
            }

            setArrCards([...items]);
        } else {
            fetch('./items.json')
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    setArrCards(data);
                });
        }
    }, []);

    return (
        <div className="App">
            <Header
                itemsInCart={arrCards}
                basketCounter={basketCounter}
                inFavorites={inFavorites}
            />
            <Routes>
                <Route
                    path="/"
                    element={
                        <HomePage
                            handleSubmitOk={handleSubmitOk}
                            handleOpenModal={handleOpenModal}
                            arrCards={arrCards}
                            handleFavoritesClick={handleFavoritesClick}
                            inFavorites={inFavorites}
                            btnName={'add phone'}
                            showBtn={false}
                            disableBtn={false}
                        />
                    }
                />
                <Route
                    path="/favorites"
                    element={
                        <FavoritesPage
                            handleSubmitOk={handleSubmitOk}
                            handleOpenModal={handleOpenModal}
                            arrCards={arrFavoriteCards}
                            handleFavoritesClick={handleFavoritesClick}
                            inFavorites={inFavorites}
                            btnName={'add phone'}
                            showBtn={false}
                            disableBtn={false}
                        />
                    }
                />
                <Route
                    path="/baskets"
                    element={
                        <BasketsPage
                            handleSubmitOk={handleSubmitOk}
                            handleOpenModal={handleOpenModal}
                            arrCards={arrBasketCards}
                            handleFavoritesClick={handleFavoritesClick}
                            inFavorites={inFavorites}
                            btnName={'buy'}
                            modalHeaderText={'delete the phone'}
                            showBtn={true}
                            disableBtn={true}
                        />
                    }
                />
                <Route path="*" element={<HomePage />} />
            </Routes>
            {openModal ? (
                <Modal
                    header={
                        !headerModalText ? 'delete the phone' : 'add the phone'
                    }
                    text={'are u sure?'}
                    actions={
                        <>
                            <Button
                                backgroundColor={'rgba(108, 40, 40, 0.41)'}
                                onClick={handleSubmit}
                                text={'Ok'}
                            />

                            <Button
                                backgroundColor={' rgba(108, 40, 40, 0.41)'}
                                onClick={() => handleCloseModal()}
                                text={'Cancel'}
                            />
                        </>
                    }
                    onClick={() => handleCloseModal()}
                />
            ) : (
                ''
            )}
        </div>
    );
}
export default App;
