import React from 'react';
import Card from '../Card';

import './style.scss';
import PropTypes from 'prop-types';
function Cards({
    handleSubmitOk,
    handleOpenModal,
    cards,
    handleFavoritesClick,
    inFavorites,
    handleBasketClick,
    btnName,
    modalHeaderText,
    showBtn,
    disableBtn
}) {
    return (
        <div className="cards_wrapper">
            {cards.map(card => {
                return (
                    <Card
                        handleSubmitOk={handleSubmitOk}
                        handleOpenModal={handleOpenModal}
                        modalHeaderText={modalHeaderText}
                        handleBasketClick={handleBasketClick}
                        handleFavoritesClick={handleFavoritesClick}
                        key={card.EAN}
                        cards={card}
                        inFavorites={inFavorites}
                        btnName={btnName}
                        showBtn={showBtn}
                        disableBtn={disableBtn}
                    />
                );
            })}
        </div>
    );
}

export default Cards;

Cards.propTypes = {
    cards: PropTypes.array,
    handleFavoritesClick: PropTypes.func,
    handleBasketClick: PropTypes.func,
    inFavorites: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    handleDeleteBtn: PropTypes.func,
    modalHeaderText: PropTypes.string,
    btnName: PropTypes.string
};

Cards.defaultProps = {
    cards: [],
    handleFavoritesClick: () => {},
    handleBasketClick: () => {},
    inFavorites: 0,
    handleDeleteBtn: () => {},
    modalHeaderText: '',
    btnName: ''
};
