import React from 'react';
import './style.scss';
import PropTypes from 'prop-types';

function Button({ backgroundColor, text, onClick, disableBtn }) {
    return (
        <>
            <button
                disabled={disableBtn}
                type="button"
                className={'buttons'}
                onClick={onClick}
                style={
                    disableBtn
                        ? { backgroundColor: 'rgba(122, 118 ,100 , 0.4)' }
                        : { backgroundColor: backgroundColor }
                }
                children={text}
            />
        </>
    );
}
export default Button;
Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
    backgroundColor: PropTypes.string
};

Button.defaultProps = {
    text: '',
    onClick: () => {},
    backgroundColor: ''
};
