import React from 'react';
import star from '../img/star1.svg';
import star2 from '../img/star2.svg';
import './style.scss';
import PropTypes from 'prop-types';

function Favorites({ inFavorites }) {
    return (
        <div className="inFavorites">
            <img src={inFavorites ? star : star2} alt="11111"></img>
            {inFavorites ? <p>{inFavorites}</p> : <p></p>}
        </div>
    );
}
export default Favorites;

Favorites.propTypes = {
    inFavorites: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

Favorites.defaultProps = {
    inFavorites: 0
};
