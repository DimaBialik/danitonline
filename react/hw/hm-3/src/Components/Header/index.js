import React from 'react';
import './style.scss';
import Favorites from '../FavoritesItems/Favorites';
import BasketItems from '../BasketItems/BasketItems';

import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';

function Header({ basketCounter, inFavorites }) {
    return (
        <>
            {' '}
            <header className="Header">
                <img className="logo" src="#" alt="" />

                <Link to="/" className="title">
                    apple store
                </Link>
                <Link to="/favorites" className="favorite">
                    <Favorites inFavorites={inFavorites} />
                </Link>
                <Link to="/baskets" className="basket">
                    <BasketItems basketCounter={basketCounter} />
                </Link>
            </header>
        </>
    );
}
export default Header;

Header.propTypes = {
    basketCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    inFavorites: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

Header.defaultProps = {
    basketCounter: 0,
    inFavorites: 0
};
