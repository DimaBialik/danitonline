import React from 'react';
import basket from '../img/basket.png';
import './style.scss';
import PropTypes from 'prop-types';

function BasketItems({ basketCounter }) {
    return (
        <div className="basketCounter">
            <img src={basket} alt="11111"></img>
            {basketCounter ? <p>{basketCounter}</p> : <p></p>}
        </div>
    );
}
export default BasketItems;

BasketItems.propTypes = {
    basketCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

BasketItems.defaultProps = {
    basketCounter: 0
};
