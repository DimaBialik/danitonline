import React from 'react';
import Header from './Components/Header';
import Cards from './Components/Cards';
import './App.scss';

class App extends React.Component {
    state = {
        arrCards: [],
        inFavorites: 0,
        basketCounter: 0
    };

    handleFavoritesClick = id => {
        const copiedArr = this.state.arrCards;
        const findElIndex = copiedArr.findIndex(({ EAN }) => EAN === id);

        if (copiedArr[findElIndex].inFavorites) {
            copiedArr[findElIndex].inFavorites = false;
            this.setState(() => {
                const nextFavoriteCounter =
                    parseInt(this.state.inFavorites) - 1;
                localStorage.setItem('inFavorites', nextFavoriteCounter);
                return { inFavorites: nextFavoriteCounter };
            });
        } else {
            copiedArr[findElIndex].inFavorites = true;
            this.setState(() => {
                const nextFavoriteCounter =
                    parseInt(this.state.inFavorites) + 1;

                localStorage.setItem('inFavorites', nextFavoriteCounter);
                return { inFavorites: nextFavoriteCounter };
            });
        }
        this.setState(() => ({
            arrCards: [...copiedArr]
        }));
    };

    handleAddToCart = id => {
        const copiedArr = this.state.arrCards;
        const findElIndex = copiedArr.findIndex(({ EAN }) => EAN === id);

        if (copiedArr[findElIndex].basketCounter) {
            copiedArr[findElIndex].basketCounter = false;
            this.setState(prev => {
                const nextBasketCounter =
                    parseFloat(this.state.basketCounter) - 1;
                localStorage.setItem('basketCounter', nextBasketCounter);
                return { basketCounter: nextBasketCounter };
            });
        } else {
            copiedArr[findElIndex].basketCounter = true;
            this.setState(prev => {
                const nextBasketCounter =
                    parseFloat(this.state.basketCounter) + 1;
                localStorage.setItem('basketCounter', nextBasketCounter);
                return { basketCounter: nextBasketCounter };
            });
        }

        this.setState(() => ({
            arrCards: [...copiedArr]
        }));

        localStorage.setItem('arrCards', JSON.stringify(this.state.arrCards));
    };
    componentDidMount() {
        if (localStorage.getItem('arrCards')) {
            const items = JSON.parse(localStorage.getItem('arrCards'));
            const basketCounter = localStorage.getItem('basketCounter');
            const inFavorites = localStorage.getItem('inFavorites');

            this.setState(() => ({
                arrCards: [...items],
                basketCounter: basketCounter,
                inFavorites: inFavorites
            }));
        } else {
            fetch('./items.json')
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    this.setState({
                        arrCards: data
                    });
                });
        }
    }
    handleDeleteBtn = index => {
        this.setState(({ arrCards }) => ({
            arrCards: [
                ...arrCards.slice(0, index),
                ...arrCards.slice(index + 1)
            ]
        }));
    };
    render() {
        const basketCounter = this.state.basketCounter;
        const inFavorites = this.state.inFavorites;
        return (
            <div className="App">
                <Header
                    itemsInCart={this.state.arrCards}
                    basketCounter={basketCounter}
                    inFavorites={inFavorites}
                />

                <Cards
                    handleDeleteBtn={this.handleDeleteBtn}
                    handleAddToCart={this.handleAddToCart}
                    cards={this.state.arrCards}
                    handleFavoritesClick={this.handleFavoritesClick}
                    inFavorites={inFavorites}
                />
            </div>
        );
    }
}
export default App;
