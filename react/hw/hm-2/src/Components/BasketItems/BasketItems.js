import React from 'react';
import basket from '../img/basket.png';
import './style.scss';
import PropTypes from 'prop-types';

class BasketItems extends React.PureComponent {
    render() {
        const { basketCounter } = this.props;

        if (basketCounter === 0) {
            return (
                <div className="basketCounter">
                    <img src={basket} alt="11111"></img>
                    <p></p>
                </div>
            );
        } else {
            return (
                <div className="basketCounter">
                    <img src={basket} alt="11111"></img>
                    <p>{basketCounter}</p>
                </div>
            );
        }
    }
}
export default BasketItems;

BasketItems.propTypes = {
    basketCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

BasketItems.defaultProps = {
    basketCounter: 0
};
