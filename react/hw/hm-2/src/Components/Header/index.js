import React from 'react';
import './style.scss';
import Favorites from '../FavoritesItems/Favorites';
import BasketItems from '../BasketItems/BasketItems';
import PropTypes from 'prop-types';

class Header extends React.Component {
    render() {
        const { basketCounter, inFavorites } = this.props;
        return (
            <header className="Header">
                <img className="logo" src="#" alt="" />
                <p className="title">apple store</p>
                <div className="favorite">
                    <Favorites inFavorites={inFavorites} />
                </div>
                <div className="basket">
                    <BasketItems basketCounter={basketCounter} />
                </div>
            </header>
        );
    }
}
export default Header;

Header.propTypes = {
    basketCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    inFavorites: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

Header.defaultProps = {
    basketCounter: 0,
    inFavorites: 0
};
