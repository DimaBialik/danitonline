import React from 'react';
import star from '../img/star1.svg';
import './style.scss';
import PropTypes from 'prop-types';

class Favorites extends React.PureComponent {
    render() {
        const { inFavorites } = this.props;

        if (inFavorites === 0) {
            return (
                <div className="inFavorites">
                    <img src={star} alt="11111"></img>
                    <p></p>
                </div>
            );
        } else {
            return (
                <div className="inFavorites">
                    <img src={star} alt="11111"></img>
                    <p>{inFavorites}</p>
                </div>
            );
        }
    }
}
export default Favorites;

Favorites.propTypes = {
    inFavorites: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

Favorites.defaultProps = {
    inFavorites: 0
};
