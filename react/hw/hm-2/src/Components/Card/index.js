import React from 'react';
import Button from '../Button';
import Modal from '../Modal';
import star from '../img/star1.svg';
import star2 from '../img/star2.svg';
import './style.scss';
import PropTypes from 'prop-types';

class Card extends React.Component {
    state = {
        openModal: false
    };
    handleOpenModal = () => {
        this.setState({ openModal: true });
    };

    handleCloseModal = () => {
        this.setState({ openModal: false });
    };
    render() {
        const {
            cards,
            handleFavoritesClick,
            handleAddToCart,
            handleDeleteBtn
        } = this.props;
        const { inFavorites, title, price, color, EAN, image } = cards;

        return (
            <div className="card">
                <div className="card_header">
                    <img
                        onClick={() => handleFavoritesClick(EAN)}
                        src={inFavorites ? star : star2}
                        alt="inFavorites__picture"
                    ></img>

                    <button className="deleteBtn" onClick={handleDeleteBtn}>
                        X
                    </button>
                </div>
                <div className="card_main">
                    <div className="card_main_info">
                        <p>{title}</p>
                        <p>{price}</p>
                        <p>{color}</p>
                        <p>{EAN}</p>
                    </div>
                    <div className="card_main_img">
                        <img src={image} alt="" />
                    </div>
                </div>
                <Button
                    text={'add phone'}
                    backgroundColor={' #cbb047'}
                    onClick={() => this.handleOpenModal()}
                />
                {this.state.openModal ? (
                    <Modal
                        header={'add phone'}
                        text={'are u sure'}
                        closeButton={true}
                        actions={
                            <>
                                <Button
                                    backgroundColor={'rgba(108, 40, 40, 0.41)'}
                                    onClick={() =>
                                        handleAddToCart(
                                            EAN,
                                            this.handleCloseModal()
                                        )
                                    }
                                    text={'Ok'}
                                />

                                <Button
                                    backgroundColor={' rgba(108, 40, 40, 0.41)'}
                                    onClick={() => this.handleCloseModal()}
                                    text={'Cancel'}
                                />
                            </>
                        }
                        onClick={e => this.handleCloseModal(e)}
                    />
                ) : (
                    ''
                )}
            </div>
        );
    }
}
export default Card;

Card.propTypes = {
    cards: PropTypes.object,
    handleFavoritesClick: PropTypes.func,

    handleDeleteBtn: PropTypes.func
};

Card.defaultProps = {
    cards: {},
    handleFavoritesClick: () => {},

    handleDeleteBtn: () => {}
};
