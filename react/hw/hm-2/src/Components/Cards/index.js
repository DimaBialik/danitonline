import React from 'react';
import Card from '../Card';

import './style.scss';
import PropTypes from 'prop-types';
class Cards extends React.Component {
    render() {
        const {
            cards,
            handleFavoritesClick,
            inFavorites,
            handleAddToCart,
            handleDeleteBtn
        } = this.props;

        return (
            <div className="cards_wrapper">
                {cards ? (
                    cards.map((card, index) => {
                        return (
                            <Card
                                handleAddToCart={handleAddToCart}
                                handleFavoritesClick={handleFavoritesClick}
                                key={card.EAN}
                                cards={card}
                                inFavorites={inFavorites}
                                handleDeleteBtn={() => handleDeleteBtn(index)}
                            />
                        );
                    })
                ) : (
                    <h2>Loading...</h2>
                )}
            </div>
        );
    }
}

export default Cards;

Cards.propTypes = {
    cards: PropTypes.array,
    handleFavoritesClick: PropTypes.func,

    inFavorites: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    handleDeleteBtn: PropTypes.func
};

Cards.defaultProps = {
    cards: [],
    handleFavoritesClick: () => {},

    inFavorites: 0,
    handleDeleteBtn: () => {}
};
