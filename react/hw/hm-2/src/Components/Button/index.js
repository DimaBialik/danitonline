import React from 'react';
import './style.scss';
import PropTypes from 'prop-types';

class Button extends React.Component {
    render() {
        const { backgroundColor, text, onClick } = this.props;
        return (
            <>
                <button
                    type="button"
                    className={'buttons'}
                    onClick={onClick}
                    style={{ backgroundColor: backgroundColor }}
                    children={text}
                />
            </>
        );
    }
}
export default Button;
Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
    backgroundColor: PropTypes.string
};

Button.defaultProps = {
    text: '',
    onClick: () => {},
    backgroundColor: ''
};
