//import React, { useState } from 'react';
import Button from '../Button';

import star from '../img/star1.svg';
import star2 from '../img/star2.svg';
import './style.scss';
import PropTypes from 'prop-types';

function Card({
    handleSubmitOk,
    handleOpenModal,
    cards,
    handleFavoritesClick,

    btnName,
    showBtn,
    disableBtn
}) {
    const { inFavorites, title, price, color, EAN, image, id } = cards;

    return (
        <div className="card" onClick={() => handleSubmitOk(id)}>
            <div className="card_header">
                <img
                    className="favorite"
                    onClick={() => handleFavoritesClick(id)}
                    src={inFavorites ? star : star2}
                    alt="inFavorites__picture"
                ></img>

                {showBtn ? (
                    <button
                        className="showBtn"
                        onClick={() => handleOpenModal(false)}
                    >
                        X
                    </button>
                ) : (
                    ''
                )}
            </div>
            <div className="card_main">
                <div className="card_main_info">
                    <p>{title}</p>
                    <p>{price}</p>
                    <p>{color}</p>
                    <p>{EAN}</p>
                </div>
                <div className="card_main_img">
                    <img src={image} alt="" />
                </div>
            </div>
            <Button
                disableBtn={disableBtn}
                text={btnName}
                backgroundColor={' #cbb047'}
                onClick={() => handleOpenModal(true)}
            />
        </div>
    );
}
export default Card;

Card.propTypes = {
    handleSubmitOk: PropTypes.func,
    handleOpenModal: PropTypes.func,
    disableBtn: PropTypes.bool,

    cards: PropTypes.object,
    handleFavoritesClick: PropTypes.func,
    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

Card.defaultProps = {
    handleSubmitOk: () => {},
    handleOpenModal: () => {},
    disableBtn: true,

    cards: {},
    handleFavoritesClick: () => {},
    btnName: '',
    showBtn: false
};
