import React from 'react';
import star from '../img/star1.svg';
import star2 from '../img/star2.svg';
import './style.scss';
import PropTypes from 'prop-types';

function Favorites({ favorites }) {
    return (
        <div className="inFavorites">
            <img
                src={favorites.length > 0 ? star : star2}
                alt="favorites"
            ></img>
            <p>{favorites.length}</p>
            {/* {favorites > 0 ? <p>{favorites.length}</p> : <p></p>} */}
        </div>
    );
}
export default Favorites;

Favorites.propTypes = {
    inFavorites: PropTypes.array
};

Favorites.defaultProps = {
    inFavorites: []
};
