import React from 'react';
import Card from '../Card';

import './style.scss';
import PropTypes from 'prop-types';
function Cards({
    handleSubmitOk,
    handleOpenModal,
    cards,
    handleFavoritesClick,

    handleBasketClick,
    btnName,
    modalHeaderText,
    showBtn,
    disableBtn
}) {
    return (
        <div className="cards_wrapper">
            {cards.map(card => {
                return (
                    <Card
                        key={card.id}
                        handleSubmitOk={handleSubmitOk}
                        handleOpenModal={handleOpenModal}
                        modalHeaderText={modalHeaderText}
                        handleBasketClick={handleBasketClick}
                        handleFavoritesClick={handleFavoritesClick}
                        cards={card}
                        btnName={btnName}
                        showBtn={showBtn}
                        disableBtn={disableBtn}
                    />
                );
            })}
        </div>
    );
}

export default React.memo(Cards);

Cards.propTypes = {
    handleSubmitOk: PropTypes.func,
    handleOpenModal: PropTypes.func,
    disableBtn: PropTypes.bool,
    modalHeaderText: PropTypes.string,
    handleBasketClick: PropTypes.func,
    cards: PropTypes.array,
    handleFavoritesClick: PropTypes.func,
    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

Cards.defaultProps = {
    handleSubmitOk: () => {},
    handleOpenModal: () => {},
    disableBtn: true,
    modalHeaderText: '',
    handleBasketClick: () => {},
    cards: [],
    handleFavoritesClick: () => {},
    btnName: '',
    showBtn: false
};
