import React from 'react';
import Button from '../Button';
import './style.scss';
import PropTypes from 'prop-types';
function Modal({ header, text, actions, closeButton, onClick }) {
    return (
        <>
            <div className="modal__container">
                {closeButton ? (
                    <div className="modal__container-header">
                        <h2 className={'modal__container-header-title'}>
                            {header}
                        </h2>
                        <Button
                            backgroundColor={'rgb(108 40 40 / 0%)'}
                            onClick={onClick}
                            text={'X'}
                        />
                    </div>
                ) : (
                    ''
                )}

                <p className={'modal__container-text'}>{text}</p>
                <div className={'modal__buttons'}>{actions}</div>
            </div>
            <div className={'modal-wrapper'} onClick={onClick} />
        </>
    );
}
export default Modal;

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    actions: PropTypes.node,
    closeButton: PropTypes.bool
};

Modal.defaultProps = {
    header: '',
    text: '',
    onClick: () => {},
    actions: <></>,
    closeButton: true
};
