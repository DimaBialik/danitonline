import React from 'react';
import basket from '../img/basket.png';
import './style.scss';
import PropTypes from 'prop-types';

function BasketItems({ baskets }) {
    return (
        <div className="basketCounter">
            <img src={basket} alt="11111"></img>
            <p>{baskets.length}</p>
        </div>
    );
}
export default BasketItems;

BasketItems.propTypes = {
    basketCounter: PropTypes.array
};

BasketItems.defaultProps = {
    basketCounter: []
};
