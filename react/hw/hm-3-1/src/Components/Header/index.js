import React from 'react';
import './style.scss';
import Favorites from '../FavoritesItems/Favorites';
import BasketItems from '../BasketItems/BasketItems';

import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';

function Header({ baskets, favorites }) {
    return (
        <>
            {' '}
            <header className="Header">
                <img className="logo" src="#" alt="" />

                <Link to="/" className="title">
                    apple store
                </Link>
                <Link to="/favorites" className="favorite">
                    <Favorites favorites={favorites} />
                </Link>
                <Link to="/baskets" className="basket">
                    <BasketItems baskets={baskets} />
                </Link>
            </header>
        </>
    );
}
export default Header;

Header.propTypes = {
    basketCounter: PropTypes.array,
    inFavorites: PropTypes.array
};

Header.defaultProps = {
    basketCounter: [],
    inFavorites: []
};
