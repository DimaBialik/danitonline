import Cards from '../../Components/Cards';
import PropTypes from 'prop-types';
export function FavoritesPage({
    handleOpenModal,
    handleSubmitOk,
    handleBasketClick,
    arrFavoriteCards,
    handleFavoritesClick,
    inFavorites,
    btnName,
    modalHeaderText,
    showBtn,
    disableBtn
}) {
    if (arrFavoriteCards.length === 0) {
        return (
            <div>
                <h2>Favorites is empty</h2>
            </div>
        );
    } else {
        return (
            <Cards
                handleOpenModal={handleOpenModal}
                handleSubmitOk={handleSubmitOk}
                disableBtn={disableBtn}
                modalHeaderText={modalHeaderText}
                handleBasketClick={handleBasketClick}
                cards={arrFavoriteCards}
                handleFavoritesClick={handleFavoritesClick}
                inFavorites={inFavorites}
                btnName={btnName}
                showBtn={showBtn}
            />
        );
    }
}

FavoritesPage.propTypes = {
    handleSubmitOk: PropTypes.func,
    handleOpenModal: PropTypes.func,
    disableBtn: PropTypes.bool,
    modalHeaderText: PropTypes.string,
    handleBasketClick: PropTypes.func,
    arrCards: PropTypes.array,
    handleFavoritesClick: PropTypes.func,
    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

FavoritesPage.defaultProps = {
    handleSubmitOk: () => {},
    handleOpenModal: () => {},
    disableBtn: true,
    modalHeaderText: '',
    handleBasketClick: () => {},
    arrCards: [],
    handleFavoritesClick: () => {},
    btnName: '',
    showBtn: false
};
