import Cards from '../../Components/Cards';
import PropTypes from 'prop-types';
export function BasketsPage({
    handleSubmitOk,
    handleOpenModal,
    handleBasketClick,
    arrCards,
    handleFavoritesClick,
    btnName,
    modalHeaderText,
    showBtn,
    disableBtn
}) {
    if (arrCards.length === 0) {
        return (
            <div>
                <h2>Basket is empty</h2>
            </div>
        );
    } else {
        return (
            <Cards
                handleSubmitOk={handleSubmitOk}
                handleOpenModal={handleOpenModal}
                disableBtn={disableBtn}
                modalHeaderText={modalHeaderText}
                handleBasketClick={handleBasketClick}
                cards={arrCards}
                handleFavoritesClick={handleFavoritesClick}
                btnName={btnName}
                showBtn={showBtn}
            />
        );
    }
}

BasketsPage.propTypes = {
    handleSubmitOk: PropTypes.func,
    handleOpenModal: PropTypes.func,
    disableBtn: PropTypes.bool,
    modalHeaderText: PropTypes.string,
    handleBasketClick: PropTypes.func,
    arrCards: PropTypes.array,
    handleFavoritesClick: PropTypes.func,
    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

BasketsPage.defaultProps = {
    handleSubmitOk: () => {},
    handleOpenModal: () => {},
    disableBtn: true,
    modalHeaderText: '',
    handleBasketClick: () => {},
    arrCards: [],
    handleFavoritesClick: () => {},
    btnName: '',
    showBtn: false
};
