import Cards from '../../Components/Cards';
import PropTypes from 'prop-types';
export function HomePage({
    handleSubmitOk,
    handleOpenModal,
    handleBasketClick,
    arrCards,
    handleFavoritesClick,
    inFavorites,
    btnName,
    modalHeaderText,
    showBtn,
    disableBtn
}) {
    return (
        <Cards
            handleSubmitOk={handleSubmitOk}
            handleOpenModal={handleOpenModal}
            disableBtn={disableBtn}
            modalHeaderText={modalHeaderText}
            handleBasketClick={handleBasketClick}
            cards={arrCards}
            handleFavoritesClick={handleFavoritesClick}
            inFavorites={inFavorites}
            btnName={btnName}
            showBtn={showBtn}
        />
    );
}
HomePage.propTypes = {
    handleSubmitOk: PropTypes.func,
    handleOpenModal: PropTypes.func,
    disableBtn: PropTypes.bool,
    modalHeaderText: PropTypes.string,
    handleBasketClick: PropTypes.func,
    arrCards: PropTypes.array,
    handleFavoritesClick: PropTypes.func,
    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

HomePage.defaultProps = {
    handleSubmitOk: () => {},
    handleOpenModal: () => {},
    disableBtn: true,
    modalHeaderText: '',
    handleBasketClick: () => {},
    arrCards: [],
    handleFavoritesClick: () => {},
    btnName: '',
    showBtn: false
};
