import React, { useEffect, useState } from 'react';
import Header from './Components/Header';
import Button from './Components/Button';
import Modal from './Components/Modal';
import { Routes, Route } from 'react-router-dom';
import { HomePage } from './Pages/HomePage';
import { FavoritesPage } from './Pages/FavoritesPage';
import { BasketsPage } from './Pages/BasketsPage';
import './App.scss';

function App() {
    const [arrCards, setArrCards] = useState([]);

    const [favorites, setFavorites] = useState([]);
    const [arrFavoriteCards, setArrFavoriteCards] = useState([]);

    const [arrBasketCards, setArrBasketCards] = useState([]);
    const [baskets, setBaskets] = useState([]);

    const [headerModalText, setHeaderModalText] = useState(false);
    const [item, setItem] = useState(0);
    const [openModal, setOpenModal] = useState(false);

    const handleOpenModal = el => {
        setHeaderModalText(el);
        setOpenModal(true);
    };

    const handleCloseModal = () => {
        setOpenModal(false);
    };
    const handleSubmitOk = item => {
        setItem(item);
    };
    const handleSubmit = () => {
        handleBasketClick(item);
        handleCloseModal();
    };

    const handleFavoritesClick = el => {
        const findElIndex = arrCards.findIndex(({ id }) => id === el);
        const findElEl = favorites.findIndex(id => id === el);

        const favoritesPage = arrCards.find(({ id }) => id === el);
        const findIndexFavor = arrFavoriteCards.findIndex(
            ({ id }) => id === el
        );

        if (arrFavoriteCards.includes(favoritesPage)) {
            setArrFavoriteCards(prev => {
                const next = [
                    ...prev.slice(0, findIndexFavor),
                    ...prev.slice(findIndexFavor + 1)
                ];
                return next;
            });
        } else {
            setArrFavoriteCards(prev => {
                const next = [...prev, favoritesPage];
                return next;
            });
        }
        if (favorites.includes(el)) {
            arrCards[findElIndex].inFavorites = false;

            setFavorites(prev => {
                const next = [
                    ...prev.slice(0, findElEl),
                    ...prev.slice(findElEl + 1)
                ];
                localStorage.setItem('Favorite', JSON.stringify(next));
                return next;
            });
        } else {
            arrCards[findElIndex].inFavorites = true;

            setFavorites(prev => {
                const next = [...prev, el];

                localStorage.setItem('Favorite', JSON.stringify(next));
                return next;
            });
        }
    };

    const handleBasketClick = el => {
        const findElEl = baskets.findIndex(id => id === el);

        const basketsPage = arrCards.find(({ id }) => id === el);
        const findIndexBaskets = arrBasketCards.findIndex(
            ({ id }) => id === el
        );
        if (arrBasketCards.includes(basketsPage)) {
            setArrBasketCards(prev => {
                const next = [
                    ...prev.slice(0, findIndexBaskets),
                    ...prev.slice(findIndexBaskets + 1)
                ];
                return next;
            });
        } else {
            setArrBasketCards(prev => {
                const next = [...prev, basketsPage];
                return next;
            });
        }
        console.log();

        if (baskets.includes(el)) {
            setBaskets(prev => {
                const next = [
                    ...prev.slice(0, findElEl),
                    ...prev.slice(findElEl + 1)
                ];
                localStorage.setItem('Baskets', JSON.stringify(next));
                return next;
            });
        } else {
            setBaskets(prev => {
                const next = [...prev, el];
                localStorage.setItem('Baskets', JSON.stringify(next));
                return next;
            });
        }
    };

    useEffect(() => {
        fetch('./items.json')
            .then(response => {
                return response.json();
            })
            .then(data => {
                setArrCards(data);
                if (localStorage.getItem('Favorite')) {
                    const favoritesArr = JSON.parse(
                        localStorage.getItem('Favorite')
                    );
                    setFavorites([...favoritesArr]);
                    const favoritesArray = favoritesArr.map(item => {
                        const favoritesItem = data.find(el => item === el.id);

                        favoritesItem.inFavorites = true;
                        return favoritesItem;
                    });
                    setArrFavoriteCards([...favoritesArray]);
                }
                if (localStorage.getItem('Baskets')) {
                    const basketsArr = JSON.parse(
                        localStorage.getItem('Baskets')
                    );

                    setBaskets([...basketsArr]);
                    const basketsArray = basketsArr.map(item => {
                        const basketsItem = data.find(el => item === el.id);

                        return basketsItem;
                    });
                    setArrBasketCards([...basketsArray]);
                }
            });
    }, []);

    return (
        <div className="App">
            <Header
                itemsInCart={arrCards}
                baskets={baskets}
                favorites={favorites}
            />
            <Routes>
                <Route
                    path="/"
                    element={
                        <HomePage
                            handleSubmitOk={handleSubmitOk}
                            handleOpenModal={handleOpenModal}
                            arrCards={arrCards}
                            handleFavoritesClick={handleFavoritesClick}
                            btnName={'add phone'}
                            showBtn={false}
                            disableBtn={false}
                        />
                    }
                />
                <Route
                    path="/favorites"
                    element={
                        <FavoritesPage
                            handleSubmitOk={handleSubmitOk}
                            handleOpenModal={handleOpenModal}
                            arrFavoriteCards={arrFavoriteCards}
                            handleFavoritesClick={handleFavoritesClick}
                            btnName={'add phone'}
                            showBtn={false}
                            disableBtn={false}
                        />
                    }
                />
                <Route
                    path="/baskets"
                    element={
                        <BasketsPage
                            handleSubmitOk={handleSubmitOk}
                            handleOpenModal={handleOpenModal}
                            arrCards={arrBasketCards}
                            handleFavoritesClick={handleFavoritesClick}
                            btnName={'buy'}
                            modalHeaderText={'delete the phone'}
                            showBtn={true}
                            disableBtn={true}
                        />
                    }
                />
                <Route path="*" element={<HomePage />} />
            </Routes>
            {openModal ? (
                <Modal
                    header={
                        !headerModalText ? 'delete the phone' : 'add the phone'
                    }
                    text={'are u sure?'}
                    actions={
                        <>
                            <Button
                                backgroundColor={'rgba(108, 40, 40, 0.41)'}
                                onClick={handleSubmit}
                                text={'Ok'}
                            />

                            <Button
                                backgroundColor={' rgba(108, 40, 40, 0.41)'}
                                onClick={() => handleCloseModal()}
                                text={'Cancel'}
                            />
                        </>
                    }
                    onClick={() => handleCloseModal()}
                />
            ) : (
                ''
            )}
        </div>
    );
}
export default App;
