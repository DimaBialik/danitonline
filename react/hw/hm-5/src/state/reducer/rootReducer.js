import { combineReducers } from 'redux';

// import {
//     persistStore,
//     persistReducer,
//     FLUSH,
//     REHYDRATE,
//     PAUSE,
//     PERSIST,
//     PURGE,
//     REGISTER
// } from 'redux-persist';
// import storage from 'redux-persist/lib/storage';

import modalReducer from './modal';
import requestReducer from './request';
import favorites from './favorites';
import basket from './basket';
// const persistConfig = {
//     key: 'root',
//     storage
// };
export const rootReducer = combineReducers({
    modal: modalReducer,
    request: requestReducer,
    favorites: favorites,
    baskets: basket
});

// const persistedReducer = persistReducer(persistConfig, rootReducer);

// const store = configureStore({
//     reducer: persistedReducer,
//     middleware: getDefaultMiddleware =>
//         getDefaultMiddleware({
//             serializableCheck: {
//                 ignoredActions: [
//                     FLUSH,
//                     REHYDRATE,
//                     PAUSE,
//                     PERSIST,
//                     PURGE,
//                     REGISTER
//                 ]
//             }
//         })
// });

// export const persistor = persistStore(store);
// export default store;
