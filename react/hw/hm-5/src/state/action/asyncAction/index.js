import { request } from '../request';

import { favoritesIdLocal, favoritesArrayLocal } from '../favorites';

import { basketsArrayLocal, basketsIdLocal } from '../basket';

export const fetchRequest = () => {
    return function (dispatch) {
        fetch('./items.json')
            .then(response => response.json())
            .then(data => {
                dispatch(request(data));
                if (localStorage.getItem('Favorite')) {
                    const getLocal = JSON.parse(
                        localStorage.getItem('Favorite')
                    );

                    dispatch(favoritesIdLocal(getLocal));
                    const favoritesArt = getLocal.map(element => {
                        const favoritesEl = data.find(
                            cards => cards.id === element
                        );
                        favoritesEl.inFavorites = true;

                        return favoritesEl;
                    });
                    dispatch(favoritesArrayLocal(favoritesArt));
                }
                if (localStorage.getItem('Basket')) {
                    const getLocal = JSON.parse(localStorage.getItem('Basket'));
                    dispatch(basketsIdLocal(getLocal));
                    const BasketArt = getLocal.map(element => {
                        const BasketEl = data.find(
                            cards => cards.id === element
                        );

                        return BasketEl;
                    });
                    dispatch(basketsArrayLocal(BasketArt));
                }
            });
    };
};
