import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
    name: Yup.string().required('Name is required'),
    lastName: Yup.string().required('Last name is required'),
    age: Yup.number()
        .typeError('Age has to be a number')
        .min(5, 'age has to be greater than 5')
        .max(100, 'age has to be less than 100')
        .required('The age is required'),
    address: Yup.string().required('Address is required'),
    tel: Yup.number()
        .typeError('Telephone number has to be a number')
        .required('Telephone number is required')
});

export default validationSchema;
