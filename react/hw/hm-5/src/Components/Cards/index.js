import React from 'react';
import Card from '../Card';

import CheckOutForm from '../Forms/CheckoutForm';
import './style.scss';
import PropTypes from 'prop-types';

function Cards({ cards, btnName, showBtn, showForm }) {
    return (
        <div className="cards_wrapper">
            <div className="cards_wrapper-cards">
                {cards.map(card => {
                    return (
                        <Card
                            key={card.id}
                            cards={card}
                            btnName={btnName}
                            showBtn={showBtn}
                            showForm={showForm}
                        />
                    );
                })}
            </div>

            {showForm ? (
                <div className="cards_form">
                    <CheckOutForm />
                </div>
            ) : (
                ''
            )}
        </div>
    );
}

export default React.memo(Cards);

Cards.propTypes = {
    cards: PropTypes.array,
    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

Cards.defaultProps = {
    cards: [],
    btnName: '',
    showBtn: false
};
