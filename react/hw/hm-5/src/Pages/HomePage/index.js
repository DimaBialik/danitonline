import Cards from '../../Components/Cards';
import PropTypes from 'prop-types';
export function HomePage({
    arrCards,

    btnName,

    showBtn,

    showForm
}) {
    return (
        <Cards
            cards={arrCards}
            btnName={btnName}
            showBtn={showBtn}
            showForm={showForm}
        />
    );
}
HomePage.propTypes = {
    arrCards: PropTypes.array,

    btnName: PropTypes.string,
    showBtn: PropTypes.bool,
    showForm: PropTypes.bool
};

HomePage.defaultProps = {
    modalHeaderText: '',

    arrCards: [],

    btnName: '',
    showBtn: false,
    showForm: false
};
