import React, { useContext } from 'react';
import Card from '../Card';

import CheckOutForm from '../Forms/CheckoutForm';
import './style.scss';
import PropTypes from 'prop-types';
import ThemeContext from '../MyContext/themeContext';

function Cards({ cards, btnName, showBtn, showForm }) {
    const { currentTheme, themes, handleCurrentTheme } =
        useContext(ThemeContext);
    // console.log(useContext(ThemeContext));

    return (
        <div className="cards_wrapper">
            <div className="cards_wrapper-cards" id={currentTheme}>
                {cards.map(card => {
                    return (
                        <Card
                            key={card.id}
                            cards={card}
                            btnName={btnName}
                            showBtn={showBtn}
                            showForm={showForm}
                        />
                    );
                })}
            </div>
            <select
                className="ChangeSelector"
                onChange={e => handleCurrentTheme(e.target.value)}
            >
                {Object.keys(themes).map(theme => (
                    <option key={theme} value={theme}>
                        {theme}
                    </option>
                ))}
            </select>{' '}
            {showForm ? (
                <div className="cards_form">
                    <CheckOutForm />
                </div>
            ) : (
                ''
            )}
        </div>
    );
}

export default React.memo(Cards);

Cards.propTypes = {
    cards: PropTypes.array,
    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

Cards.defaultProps = {
    cards: [],
    btnName: '',
    showBtn: false
};
