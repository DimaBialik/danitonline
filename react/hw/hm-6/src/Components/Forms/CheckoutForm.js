import { Formik, Form } from 'formik';
import initialValues from './initialValues';
import CustomForm from './CustomField/CustomForm';
import validationSchema from './validationSchema';
import styles from './CheckoutForm.module.scss';
import { useSelector, useDispatch } from 'react-redux';
import { basketsBuy } from '../../state/action/basket';

const CheckOutForm = () => {
    const dispatch = useDispatch();
    const { basketArrayCards } = useSelector(state => state.baskets);
    const orderCards = basketArrayCards.map(element => {
        return element.title;
    });
    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={value => {
                dispatch(basketsBuy());
                console.log(
                    `u buy ${orderCards.length} iphone ==> ${orderCards}`
                );
                console.log(
                    `u info ==> name:${value.name}, lastName:${value.lastName}, address:${value.address}, tel:${value.tel}`
                );
                localStorage.setItem('Basket', JSON.stringify([]));
            }}
        >
            {props => {
                return (
                    <Form className={styles.checkoutWrapper}>
                        <CustomForm
                            type="text"
                            placeholder="name"
                            name="name"
                        />
                        <CustomForm
                            type="text"
                            placeholder="last Name"
                            name="lastName"
                        />
                        <CustomForm type="text" placeholder="age" name="age" />
                        <CustomForm
                            type="text"
                            placeholder="address"
                            name="address"
                        />
                        <CustomForm type="text" placeholder="tel" name="tel" />
                        <button disabled={!props.isValid} type="submit">
                            order
                        </button>
                    </Form>
                );
            }}
        </Formik>
    );
};

export default CheckOutForm;
