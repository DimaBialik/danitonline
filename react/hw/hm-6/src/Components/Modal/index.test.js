import { render, screen } from '@testing-library/react';
import Modal from './index';
import { Provider } from 'react-redux';
import { legacy_createStore as createStore } from 'redux';
import { rootReducer } from '../../state/reducer/rootReducer';

describe('Modal component', () => {
    const store = createStore(rootReducer);

    it('should render', () => {
        const { container } = render(
            <Provider store={store}>
                <Modal />
            </Provider>
        );
        expect(container).toMatchSnapshot();
    });

    it('should render modal with title', () => {
        render(
            <Provider store={store}>
                <Modal header={'Add to Card'} />
            </Provider>
        );
        screen.getByText(/Add to Card/i);
    });
});
