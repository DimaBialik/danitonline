import { useSelector } from 'react-redux';
import React from 'react';
import star from '../img/star1.svg';
import star2 from '../img/star2.svg';
import './style.scss';

function Favorites() {
    const { favorites } = useSelector(state => state.favorites);
    return (
        <div className="inFavorites">
            <img
                src={favorites.length > 0 ? star : star2}
                alt="favorites"
            ></img>

            {favorites.length > 0 ? <p>{favorites.length}</p> : <p></p>}
        </div>
    );
}
export default Favorites;
