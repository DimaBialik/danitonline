import { render, screen } from '@testing-library/react';

import Button from './index';

describe('<Button/> component', () => {
    const { container } = render(<Button></Button>);
    it('should render', () => {
        expect(container).toMatchSnapshot();
    });
    it('should render button tag', () => {
        render(<Button></Button>);
        screen.getByRole('button');
    });

    it('should render button with text', () => {
        render(<Button text="Add to Card"></Button>);
        screen.getByText(/Add to Card/);
    });
});
