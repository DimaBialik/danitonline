import { useState } from 'react';
import ContextTheme from './themeContext';

const THEMES = {
    grow: '',
    column: 'active'
};

export default function ThemeProvider({ children }) {
    const [currentTheme, setCurrentTheme] = useState(THEMES.grow);

    const handleCurrentTheme = theme => {
        console.log(THEMES[theme]);
        setCurrentTheme(THEMES[theme]);
    };

    return (
        <ContextTheme.Provider
            value={{ currentTheme, themes: THEMES, handleCurrentTheme }}
        >
            {children}
        </ContextTheme.Provider>
    );
}
