import Cards from '../../Components/Cards';
import PropTypes from 'prop-types';
export function FavoritesPage({
    arrFavoriteCards,

    btnName,

    showBtn,

    showForm
}) {
    if (arrFavoriteCards.length === 0) {
        return (
            <div>
                <h2>Favorites is empty</h2>
            </div>
        );
    } else {
        return (
            <Cards
                cards={arrFavoriteCards}
                btnName={btnName}
                showBtn={showBtn}
                showForm={showForm}
            />
        );
    }
}

FavoritesPage.propTypes = {
    arrCards: PropTypes.array,

    btnName: PropTypes.string,
    showBtn: PropTypes.bool,
    showForm: PropTypes.bool
};

FavoritesPage.defaultProps = {
    modalHeaderText: '',

    arrCards: [],

    btnName: '',
    showBtn: false,
    showForm: false
};
