import Cards from '../../Components/Cards';
import PropTypes from 'prop-types';
export function BasketsPage({
    arrCards,

    btnName,

    showBtn,

    showForm
}) {
    if (arrCards.length === 0) {
        return (
            <div>
                <h2>Basket is empty</h2>
            </div>
        );
    } else {
        return (
            <Cards
                cards={arrCards}
                btnName={btnName}
                showBtn={showBtn}
                showForm={showForm}
            />
        );
    }
}

BasketsPage.propTypes = {
    arrCards: PropTypes.array,

    btnName: PropTypes.string,
    showBtn: PropTypes.bool,
    showForm: PropTypes.bool
};

BasketsPage.defaultProps = {
    modalHeaderText: '',

    arrCards: [],

    btnName: '',
    showBtn: false,
    showForm: PropTypes.bool
};
