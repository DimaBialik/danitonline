import {
    FAVORITES_ARRAY_LOCAL,
    FAVORITES_ID_LOCAL,
    FAVORITES_ID,
    FAVORITES_ARRAY_CARDS,
    FAVORITES_ARRAY_CARDS_DELETE,
    FAVORITES_ID_DELETE
} from '../action/favorites';
import favorites from './favorites';
const initialStateAdd = {
    favorites: [],
    favoritesArrayCards: []
};
const initialStateDelete = {
    favorites: [1, 2, 3],
    favoritesArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }]
};

describe('Favorites delete works', () => {
    test('should return the initial state', () => {
        expect(
            favorites(initialStateDelete, {
                type: undefined
            })
        ).toEqual({
            favorites: [1, 2, 3],
            favoritesArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }]
        });
    });

    test('should delete basketId work', () => {
        expect(
            favorites(initialStateDelete, {
                type: FAVORITES_ID_DELETE,
                payload: 2
            })
        ).toEqual({
            favorites: [1, 2],
            favoritesArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }]
        });
    });
    test('should delete basketArray work', () => {
        expect(
            favorites(initialStateDelete, {
                type: FAVORITES_ARRAY_CARDS_DELETE,
                payload: { title: 'Iphone', EAN: 123, color: 'black' }
            })
        ).toEqual({
            favorites: [1, 2, 3],
            favoritesArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }]
        });
    });
});

describe('Favorites add works', () => {
    test('should return the initial state', () => {
        expect(
            favorites(initialStateAdd, {
                type: undefined
            })
        ).toEqual({
            favorites: [],
            favoritesArrayCards: []
        });
    });

    test('should add FavoritesId work', () => {
        expect(
            favorites(initialStateAdd, {
                type: FAVORITES_ID,
                payload: 1
            })
        ).toEqual({
            favorites: [1],
            favoritesArrayCards: []
        });
    });
    test('should add favoritesIdLocal work', () => {
        expect(
            favorites(initialStateAdd, {
                type: FAVORITES_ID_LOCAL,
                payload: [1]
            })
        ).toEqual({
            favorites: [1],
            favoritesArrayCards: []
        });
    });

    test('should add favoritesArray work', () => {
        expect(
            favorites(initialStateAdd, {
                type: FAVORITES_ARRAY_CARDS,
                payload: { title: 'Iphone', EAN: 123, color: 'black' }
            })
        ).toEqual({
            favorites: [],
            favoritesArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }]
        });
    });
    test('should add favoritesArrayLocal work', () => {
        expect(
            favorites(initialStateAdd, {
                type: FAVORITES_ARRAY_LOCAL,
                payload: [{ title: 'Iphone', EAN: 123, color: 'black' }]
            })
        ).toEqual({
            favorites: [],
            favoritesArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }]
        });
    });
});
