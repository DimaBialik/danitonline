import { MODAL_CLOSE, MODAL_OPEN } from '../action/modal';
import modalReducer from './modal';

const initialState = {
    modal: false,
    modalHeader: ''
};

describe('Modal works', () => {
    test('should return the initial state', () => {
        expect(
            modalReducer(initialState, {
                type: undefined,
                payload: false,
                modalHeader: ''
            })
        ).toEqual({
            modal: false,
            modalHeader: ''
        });
    });

    test('should modal  open', () => {
        expect(
            modalReducer(initialState, {
                type: MODAL_OPEN,
                payload: true
            })
        ).toEqual({
            modal: true
        });
    });
    test('should modal close', () => {
        expect(
            modalReducer(initialState, {
                type: MODAL_CLOSE,
                payload: false
            })
        ).toEqual({
            modal: false
        });
    });
});
