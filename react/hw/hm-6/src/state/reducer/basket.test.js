import basket from './basket';
import {
    BASKET_ARRAY_CARDS,
    BASKET_ARRAY_CARDS_DELETE,
    BASKET_ID,
    BASKET_ID_DELETE,
    BASKET_CARD,
    BASKET_ARRAY_LOCAL,
    BASKET_ID_LOCAL,
    BASKET_BUY
} from '../action/basket';

const initialStateAdd = {
    basket: [],
    basketArrayCards: [],
    basketCards: []
};
const initialStateDelete = {
    basket: [1, 2, 3],
    basketArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }],
    basketCards: [{ title: 'Iphone', EAN: 123, color: 'black' }]
};

describe('Basket delete works', () => {
    test('should return the initial state', () => {
        expect(
            basket(initialStateDelete, {
                type: undefined
            })
        ).toEqual({
            basket: [1, 2, 3],
            basketArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }],
            basketCards: [{ title: 'Iphone', EAN: 123, color: 'black' }]
        });
    });

    test('should delete basketId work', () => {
        expect(
            basket(initialStateDelete, {
                type: BASKET_ID_DELETE,
                payload: 2
            })
        ).toEqual({
            basket: [1, 2],
            basketArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }],
            basketCards: [{ title: 'Iphone', EAN: 123, color: 'black' }]
        });
    });
    test('should delete basketArray work', () => {
        expect(
            basket(initialStateDelete, {
                type: BASKET_ARRAY_CARDS_DELETE,
                payload: { title: 'Iphone', EAN: 123, color: 'black' }
            })
        ).toEqual({
            basket: [1, 2, 3],
            basketArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }],
            basketCards: [{ title: 'Iphone', EAN: 123, color: 'black' }]
        });
    });

    // test('should add basketArray work', () => {
    //     expect(
    //         basket(initialStateAdd, {
    //             type: BASKET_ARRAY_CARDS,
    //             payload: { title: 'Iphone', EAN: 123, color: 'black' }
    //         })
    //     ).toEqual({
    //         basket: [],
    //         basketArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }],
    //         basketCards: []
    //     });
    // });
    // test('should add basketArrayLocal work', () => {
    //     expect(
    //         basket(initialStateAdd, {
    //             type: BASKET_ARRAY_LOCAL,
    //             payload: [{ title: 'Iphone', EAN: 123, color: 'black' }]
    //         })
    //     ).toEqual({
    //         basket: [],
    //         basketArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }],
    //         basketCards: []
    //     });
    // });
    // test('should  basketCard work', () => {
    //     expect(
    //         basket(initialStateAdd, {
    //             type: BASKET_CARD,
    //             payload: { title: 'Iphone', EAN: 123, color: 'black' }
    //         })
    //     ).toEqual({
    //         basket: [],
    //         basketArrayCards: [],
    //         basketCards: { title: 'Iphone', EAN: 123, color: 'black' }
    //     });
    // });
});

describe('Basket add works', () => {
    test('should return the initial state', () => {
        expect(
            basket(initialStateAdd, {
                type: undefined
            })
        ).toEqual({
            basket: [],
            basketArrayCards: [],
            basketCards: []
        });
    });
    test('should basket buy', () => {
        expect(
            basket(initialStateAdd, {
                type: BASKET_BUY
            })
        ).toEqual({
            basket: [],
            basketArrayCards: [],
            basketCards: []
        });
    });
    test('should add basketId work', () => {
        expect(
            basket(initialStateAdd, {
                type: BASKET_ID,
                payload: 1
            })
        ).toEqual({
            basket: [1],
            basketArrayCards: [],
            basketCards: []
        });
    });
    test('should add basketIdLocal work', () => {
        expect(
            basket(initialStateAdd, {
                type: BASKET_ID_LOCAL,
                payload: [1]
            })
        ).toEqual({
            basket: [1],
            basketArrayCards: [],
            basketCards: []
        });
    });

    test('should add basketArray work', () => {
        expect(
            basket(initialStateAdd, {
                type: BASKET_ARRAY_CARDS,
                payload: { title: 'Iphone', EAN: 123, color: 'black' }
            })
        ).toEqual({
            basket: [],
            basketArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }],
            basketCards: []
        });
    });
    test('should add basketArrayLocal work', () => {
        expect(
            basket(initialStateAdd, {
                type: BASKET_ARRAY_LOCAL,
                payload: [{ title: 'Iphone', EAN: 123, color: 'black' }]
            })
        ).toEqual({
            basket: [],
            basketArrayCards: [{ title: 'Iphone', EAN: 123, color: 'black' }],
            basketCards: []
        });
    });
    test('should  basketCard work', () => {
        expect(
            basket(initialStateAdd, {
                type: BASKET_CARD,
                payload: { title: 'Iphone', EAN: 123, color: 'black' }
            })
        ).toEqual({
            basket: [],
            basketArrayCards: [],
            basketCards: { title: 'Iphone', EAN: 123, color: 'black' }
        });
    });
});
