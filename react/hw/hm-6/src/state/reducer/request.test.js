import requestReducer from './request';
import { REQUEST } from '../action/request';

const initialState = {
    arrCards: []
};

describe('Request works', () => {
    test('should return the initial state', () => {
        expect(
            requestReducer(initialState, {
                type: undefined
            })
        ).toEqual({
            arrCards: []
        });
    });
    test('should request work', () => {
        expect(
            requestReducer(initialState, {
                type: REQUEST,
                payload: [
                    {
                        title: 'Iphone 11',
                        price: '500$',
                        image: './images/goodsImg/iphine11(black).jpg',
                        EAN: '32112',
                        color: 'black',
                        inFavorites: false,
                        basketCounter: false,
                        id: 1
                    }
                ]
            })
        ).toEqual({
            arrCards: [
                {
                    title: 'Iphone 11',
                    price: '500$',
                    image: './images/goodsImg/iphine11(black).jpg',
                    EAN: '32112',
                    color: 'black',
                    inFavorites: false,
                    basketCounter: false,
                    id: 1
                }
            ]
        });
    });
});
