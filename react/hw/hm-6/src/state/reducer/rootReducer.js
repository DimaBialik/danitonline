import { combineReducers } from 'redux';

import modalReducer from './modal';
import requestReducer from './request';
import favorites from './favorites';
import basket from './basket';

export const rootReducer = combineReducers({
    modal: modalReducer,
    request: requestReducer,
    favorites: favorites,
    baskets: basket
});
