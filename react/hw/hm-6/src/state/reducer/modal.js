import { MODAL_CLOSE, MODAL_OPEN } from '../action/modal';

const initialState = {
    modal: false,
    modalHeader: ''
};
const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case MODAL_CLOSE:
            return {
                ...state,
                modal: action.payload,
                modalHeader: action.modalHeaders
            };
        case MODAL_OPEN:
            return {
                ...state,
                modal: action.payload,
                modalHeader: action.modalHeaders
            };

        default:
            return state;
    }
};

export default modalReducer;
