export const MODAL_OPEN = 'MODAL_OPEN';
export const MODAL_CLOSE = 'MODAL_CLOSE';

export function modalOpen(value, header) {
    return {
        type: MODAL_OPEN,
        payload: value,
        modalHeaders: header
    };
}
export function modalClose(value, header) {
    return {
        type: MODAL_CLOSE,
        payload: value,
        modalHeaders: header
    };
}
