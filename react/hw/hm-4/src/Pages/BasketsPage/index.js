import Cards from '../../Components/Cards';
import PropTypes from 'prop-types';
export function BasketsPage({
    arrCards,

    btnName,

    showBtn,
    disableBtn
}) {
    if (arrCards.length === 0) {
        return (
            <div>
                <h2>Basket is empty</h2>
            </div>
        );
    } else {
        return (
            <Cards
                disableBtn={disableBtn}
                cards={arrCards}
                btnName={btnName}
                showBtn={showBtn}
            />
        );
    }
}

BasketsPage.propTypes = {
    modalHeaderText: PropTypes.string,

    arrCards: PropTypes.array,

    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

BasketsPage.defaultProps = {
    disableBtn: true,

    arrCards: [],

    btnName: '',
    showBtn: false
};
