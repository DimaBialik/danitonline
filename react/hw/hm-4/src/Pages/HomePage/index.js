import Cards from '../../Components/Cards';
import PropTypes from 'prop-types';
export function HomePage({
    arrCards,

    btnName,

    showBtn,
    disableBtn
}) {
    return (
        <Cards
            disableBtn={disableBtn}
            cards={arrCards}
            btnName={btnName}
            showBtn={showBtn}
        />
    );
}
HomePage.propTypes = {
    modalHeaderText: PropTypes.string,

    arrCards: PropTypes.array,

    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

HomePage.defaultProps = {
    disableBtn: true,

    arrCards: [],

    btnName: '',
    showBtn: false
};
