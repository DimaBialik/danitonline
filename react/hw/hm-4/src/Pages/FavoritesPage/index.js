import Cards from '../../Components/Cards';
import PropTypes from 'prop-types';
export function FavoritesPage({
    arrFavoriteCards,

    btnName,

    showBtn,
    disableBtn
}) {
    if (arrFavoriteCards.length === 0) {
        return (
            <div>
                <h2>Favorites is empty</h2>
            </div>
        );
    } else {
        return (
            <Cards
                disableBtn={disableBtn}
                cards={arrFavoriteCards}
                btnName={btnName}
                showBtn={showBtn}
            />
        );
    }
}

FavoritesPage.propTypes = {
    modalHeaderText: PropTypes.string,

    arrFavoriteCards: PropTypes.array,

    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

FavoritesPage.defaultProps = {
    disableBtn: true,

    arrFavoriteCards: [],

    btnName: '',
    showBtn: false
};
