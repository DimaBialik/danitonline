import React from 'react';
import Card from '../Card';

import './style.scss';
import PropTypes from 'prop-types';

function Cards({ cards, btnName, showBtn, disableBtn }) {
    return (
        <div className="cards_wrapper">
            {cards.map(card => {
                return (
                    <Card
                        key={card.id}
                        cards={card}
                        btnName={btnName}
                        showBtn={showBtn}
                        disableBtn={disableBtn}
                    />
                );
            })}
        </div>
    );
}

export default React.memo(Cards);

Cards.propTypes = {
    disableBtn: PropTypes.bool,
    cards: PropTypes.array,
    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

Cards.defaultProps = {
    disableBtn: true,
    cards: [],
    btnName: '',
    showBtn: false
};
