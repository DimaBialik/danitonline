//import React, { useState } from 'react';
import Button from '../Button';

import star from '../img/star1.svg';
import star2 from '../img/star2.svg';
import './style.scss';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';

import { modalOpen } from '../../state/action/modal';
import { basketsCard } from '../../state/action/basket';
import {
    favoritesArray,
    favoritesId,
    favoritesArrayDelete,
    favoritesIdDelete
} from '../../state/action/favorites';

function Card({ cards, btnName, showBtn, disableBtn }) {
    const { inFavorites, title, price, color, EAN, image } = cards;
    const { favorites, favoritesArrayCards } = useSelector(
        state => state.favorites
    );

    const dispatch = useDispatch();
    const handleOpenModal = (el, text) => {
        dispatch(modalOpen(el, text));
    };
    const handleSubmitOk = el => {
        dispatch(basketsCard(el));
    };
    const handleFavoritesClick = el => {
        const findElEl = favorites.findIndex(id => id === el.id);
        const findIndexFavor = favoritesArrayCards.findIndex(
            ({ id }) => id === el.id
        );

        if (favoritesArrayCards.includes(el)) {
            dispatch(favoritesArrayDelete(findIndexFavor));
        } else {
            dispatch(favoritesArray(el));
        }

        if (favorites.includes(el.id)) {
            cards.inFavorites = false;
            dispatch(favoritesIdDelete(findElEl));
            localStorage.setItem(
                'Favorite',
                JSON.stringify([
                    ...favorites.slice(0, findElEl),
                    ...favorites.slice(findElEl + 1)
                ])
            );
        } else {
            cards.inFavorites = true;

            dispatch(favoritesId(el.id));
            console.log(el.id);
            localStorage.setItem(
                'Favorite',
                JSON.stringify([...favorites, el.id])
            );
        }
    };

    return (
        <div className="card" onClick={() => handleSubmitOk(cards)}>
            <div className="card_header">
                <img
                    className="favorite"
                    onClick={() => handleFavoritesClick(cards)}
                    src={inFavorites ? star : star2}
                    alt="inFavorites__picture"
                ></img>

                {showBtn ? (
                    <button
                        className="showBtn"
                        onClick={() =>
                            handleOpenModal(true, 'delete the phone')
                        }
                    >
                        X
                    </button>
                ) : (
                    ''
                )}
            </div>
            <div className="card_main">
                <div className="card_main_info">
                    <p>{title}</p>
                    <p>{price}</p>
                    <p>{color}</p>
                    <p>{EAN}</p>
                </div>
                <div className="card_main_img">
                    <img src={image} alt="" />
                </div>
            </div>
            <Button
                disableBtn={disableBtn}
                text={btnName}
                backgroundColor={' #cbb047'}
                onClick={() => handleOpenModal(true, 'add the phone')}
            />
        </div>
    );
}
export default Card;

Card.propTypes = {
    disableBtn: PropTypes.bool,
    cards: PropTypes.object,
    btnName: PropTypes.string,
    showBtn: PropTypes.bool
};

Card.defaultProps = {
    disableBtn: true,
    cards: {},
    btnName: '',
    showBtn: false
};
