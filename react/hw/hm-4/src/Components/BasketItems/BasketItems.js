import { useSelector } from 'react-redux';
import React from 'react';
import basket from '../img/basket.png';
import './style.scss';
import PropTypes from 'prop-types';

function BasketItems() {
    const basketCount = useSelector(state => state.baskets.basket);
    return (
        <div className="basketCounter">
            <img src={basket} alt="11111"></img>
            {basketCount.length > 0 ? <p>{basketCount.length}</p> : <p></p>}
        </div>
    );
}
export default BasketItems;

BasketItems.propTypes = {
    basketCount: PropTypes.array
};

BasketItems.defaultProps = {
    basketCount: []
};
