import React, { useEffect } from 'react';
import Header from './Components/Header';
import Button from './Components/Button';
import Modal from './Components/Modal';
import { Routes, Route } from 'react-router-dom';
import { HomePage } from './Pages/HomePage';
import { FavoritesPage } from './Pages/FavoritesPage';
import { BasketsPage } from './Pages/BasketsPage';
import './App.scss';

import { useSelector, useDispatch } from 'react-redux';
import { modalClose } from './state/action/modal';
import { fetchRequest } from './state/action/asyncAction/index';
import {
    basketsArray,
    basketsId,
    basketsArrayDelete,
    basketsIdDelete
} from './state/action/basket';

function App() {
    const { modal, modalHeader } = useSelector(state => state.modal);

    const cards = useSelector(state => state.request.arrCards);

    const { favorites, favoritesArrayCards } = useSelector(
        state => state.favorites
    );
    const { basketCards, basket, basketArrayCards } = useSelector(
        state => state.baskets
    );

    const dispatch = useDispatch();

    const handleCloseModal = () => {
        dispatch(modalClose(false));
    };

    const handleSubmit = () => {
        handleBasketClick(basketCards);

        handleCloseModal();
    };

    const handleBasketClick = el => {
        if (basketArrayCards.includes(el)) {
            const findIndexBaskets = basketArrayCards.findIndex(
                ({ id }) => id === el.id
            );
            dispatch(basketsArrayDelete(findIndexBaskets));
        } else {
            dispatch(basketsArray(el));
        }

        if (basket.includes(el.id)) {
            const findElEl = basket.findIndex(id => id === el.id);
            dispatch(basketsIdDelete(findElEl));
            localStorage.setItem(
                'Basket',
                JSON.stringify([
                    ...basket.slice(0, findElEl),
                    ...basket.slice(findElEl + 1)
                ])
            );
        } else {
            dispatch(basketsId(el.id));
            localStorage.setItem('Basket', JSON.stringify([...basket, el.id]));
        }
    };

    useEffect(() => {
        dispatch(fetchRequest());
    }, [dispatch]);

    return (
        <div className="App">
            <Header baskets={basket} favorites={favorites} />
            <Routes>
                <Route
                    path="/"
                    element={
                        <HomePage
                            arrCards={cards}
                            btnName={'add phone'}
                            showBtn={false}
                            disableBtn={false}
                        />
                    }
                />
                <Route
                    path="/favorites"
                    element={
                        <FavoritesPage
                            arrFavoriteCards={favoritesArrayCards}
                            btnName={'add phone'}
                            showBtn={false}
                            disableBtn={false}
                        />
                    }
                />
                <Route
                    path="/baskets"
                    element={
                        <BasketsPage
                            arrCards={basketArrayCards}
                            btnName={'buy'}
                            modalHeaderText={'delete the phone'}
                            showBtn={true}
                            disableBtn={true}
                        />
                    }
                />
                <Route path="*" element={<HomePage />} />
            </Routes>
            {modal ? (
                <Modal
                    header={modalHeader}
                    text={'are u sure?'}
                    actions={
                        <>
                            <Button
                                backgroundColor={'rgba(108, 40, 40, 0.41)'}
                                onClick={handleSubmit}
                                text={'Ok'}
                            />

                            <Button
                                backgroundColor={' rgba(108, 40, 40, 0.41)'}
                                onClick={() => handleCloseModal()}
                                text={'Cancel'}
                            />
                        </>
                    }
                    onClick={() => handleCloseModal()}
                />
            ) : (
                ''
            )}
        </div>
    );
}
export default App;
