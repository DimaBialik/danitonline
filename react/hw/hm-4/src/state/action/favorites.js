export const FAVORITES_ID = 'FAVORITES_ID';
export const FAVORITES_ARRAY_CARDS = 'FAVORITES_ARRAY_CARDS';
export const FAVORITES_ID_DELETE = 'FAVORITES_ID_DELETE';
export const FAVORITES_ARRAY_CARDS_DELETE = 'FAVORITES_ARRAY_CARDS_DELETE';
export const FAVORITES_ID_LOCAL = 'FAVORITES_LOCAL';
export const FAVORITES_ARRAY_LOCAL = 'FAVORITES_ARRAY_LOCAL';

export function favoritesId(payload) {
    return {
        type: FAVORITES_ID,
        payload
    };
}

export function favoritesArray(payload) {
    return {
        type: FAVORITES_ARRAY_CARDS,
        payload
    };
}

export function favoritesArrayDelete(payload) {
    return {
        type: FAVORITES_ARRAY_CARDS_DELETE,
        payload
    };
}
export function favoritesIdDelete(payload) {
    return {
        type: FAVORITES_ID_DELETE,
        payload
    };
}

export function favoritesIdLocal(payload) {
    return {
        type: FAVORITES_ID_LOCAL,
        payload
    };
}
export function favoritesArrayLocal(payload) {
    return {
        type: FAVORITES_ARRAY_LOCAL,
        payload
    };
}
