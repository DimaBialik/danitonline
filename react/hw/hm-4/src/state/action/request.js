export const REQUEST = 'REQUEST';

export function request(value) {
    return {
        type: REQUEST,
        payload: value
    };
}
