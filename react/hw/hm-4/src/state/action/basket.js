export const BASKET_ID = 'BASKET_ID';
export const BASKET_CARD = 'BASKET_CARD';
export const BASKET_ARRAY_CARDS = 'BASKET_ARRAY_CARDS';
export const BASKET_ID_DELETE = 'BASKET_ID_DELETE';
export const BASKET_ARRAY_CARDS_DELETE = 'BASKET_ARRAY_CARDS_DELETE';
export const BASKET_ID_LOCAL = 'BASKET_ID_LOCAL';
export const BASKET_ARRAY_LOCAL = 'BASKET_ARRAY_LOCAL';
export function basketsCard(payload) {
    return {
        type: BASKET_CARD,
        payload
    };
}
export function basketsId(payload) {
    return {
        type: BASKET_ID,
        payload
    };
}

export function basketsArray(payload) {
    return {
        type: BASKET_ARRAY_CARDS,
        payload
    };
}
export function basketsArrayDelete(payload) {
    return {
        type: BASKET_ARRAY_CARDS_DELETE,
        payload
    };
}
export function basketsIdDelete(payload) {
    return {
        type: BASKET_ID_DELETE,
        payload
    };
}
export function basketsIdLocal(payload) {
    return {
        type: BASKET_ID_LOCAL,
        payload
    };
}
export function basketsArrayLocal(payload) {
    return {
        type: BASKET_ARRAY_LOCAL,
        payload
    };
}
