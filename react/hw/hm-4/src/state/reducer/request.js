import { REQUEST } from '../action/request';

const initialState = {
    arrCards: []
};
const requestReducer = (state = initialState, action) => {
    switch (action.type) {
        case REQUEST:
            return {
                ...state,
                arrCards: action.payload
            };

        default:
            return state;
    }
};

export default requestReducer;
