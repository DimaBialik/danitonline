import {
    FAVORITES_ARRAY_LOCAL,
    FAVORITES_ID_LOCAL,
    FAVORITES_ID,
    FAVORITES_ARRAY_CARDS,
    FAVORITES_ARRAY_CARDS_DELETE,
    FAVORITES_ID_DELETE
} from '../action/favorites';

const initialState = {
    favorites: [],
    favoritesArrayCards: []
};
const favorites = (state = initialState, action) => {
    switch (action.type) {
        case FAVORITES_ARRAY_CARDS:
            return {
                ...state,
                favoritesArrayCards: [
                    ...state.favoritesArrayCards,
                    action.payload
                ]
            };
        case FAVORITES_ID:
            return {
                ...state,
                favorites: [...state.favorites, action.payload]
            };
        case FAVORITES_ID_LOCAL:
            return {
                ...state,
                favorites: action.payload
            };
        case FAVORITES_ARRAY_LOCAL:
            return {
                ...state,
                favoritesArrayCards: action.payload
            };

        case FAVORITES_ARRAY_CARDS_DELETE:
            return {
                ...state,
                favoritesArrayCards: [
                    ...state.favoritesArrayCards.slice(0, action.payload),
                    ...state.favoritesArrayCards.slice(action.payload + 1)
                ]
            };
        case FAVORITES_ID_DELETE:
            return {
                ...state,
                favorites: [
                    ...state.favorites.slice(0, action.payload),
                    ...state.favorites.slice(action.payload + 1)
                ]
            };

        default:
            return state;
    }
};

export default favorites;
