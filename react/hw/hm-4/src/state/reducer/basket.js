import {
    BASKET_ARRAY_CARDS,
    BASKET_ARRAY_CARDS_DELETE,
    BASKET_ID,
    BASKET_ID_DELETE,
    BASKET_CARD,
    BASKET_ARRAY_LOCAL,
    BASKET_ID_LOCAL
} from '../action/basket';

const initialState = {
    basket: [],
    basketArrayCards: [],
    basketCards: []
};
const basket = (state = initialState, action) => {
    switch (action.type) {
        case BASKET_ID_LOCAL:
            return {
                ...state,
                basket: action.payload
            };
        case BASKET_ARRAY_LOCAL:
            return {
                ...state,
                basketArrayCards: action.payload
            };
        case BASKET_CARD:
            return {
                ...state,
                basketCards: action.payload
            };
        case BASKET_ARRAY_CARDS:
            return {
                ...state,
                basketArrayCards: [...state.basketArrayCards, action.payload]
            };

        case BASKET_ID:
            return {
                ...state,
                basket: [...state.basket, action.payload]
            };

        case BASKET_ARRAY_CARDS_DELETE:
            return {
                ...state,
                basketArrayCards: [
                    ...state.basketArrayCards.slice(0, action.payload),
                    ...state.basketArrayCards.slice(action.payload + 1)
                ]
            };
        case BASKET_ID_DELETE:
            return {
                ...state,
                basket: [
                    ...state.basket.slice(0, action.payload),
                    ...state.basket.slice(action.payload + 1)
                ]
            };

        default:
            return state;
    }
};

export default basket;
