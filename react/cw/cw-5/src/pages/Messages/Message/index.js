import React from "react";

import { Link } from "react-router-dom";

import NotFound from "../../../pages/NotFound";

function Message({ match, history, messages }) {
  const { id } = match.params;

  const message = messages.find((m) => m.id === parseInt(id));

  if (typeof message === "undefined") {
    return <NotFound />;
  }

  const handleBackClick = () => {
    history.goBack();
  };

  return (
    <div>
      <Link to="/messages">/Messages</Link> / {message.id} <br />
      <button type="button" onClick={handleBackClick}>
        Go back
      </button>
      <h3>
        ({message.id}) {message.title}
      </h3>
      <p>{message.description}</p>
    </div>
  );
}

export default Message;
