import React from "react";

import { Switch, Route } from "react-router-dom";

import NotFound from "../../pages/NotFound";

import Message from "./Message";
import MessageList from "./MessageList";

function Messages({ match, messages }) {
  return (
    <Switch>
      <Route exact path={match.url}>
        <MessageList messages={messages} />
      </Route>
      <Route
        exact
        path={`${match.url}/:id(\\d+)`}
        render={(props) => <Message {...props} messages={messages} />}
      />
      <Route path="*" component={NotFound} />
    </Switch>
  );
}

export default Messages;
