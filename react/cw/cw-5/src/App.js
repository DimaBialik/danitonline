import React, { Component } from "react";

import Header from "./components/Header";
import Footer from "./components/Footer/Footer";

import Home from "./pages/Home";
import Messages from "./pages/Messages";
import Sent from "./pages/Sent";
import NotFound from "./pages/NotFound";

import { Switch, Route, Redirect } from "react-router-dom";

import "./App.css";

const EMAILS = [
  {
    id: 1,
    title: "Title 1",
    description: "Description 1",
  },
  {
    id: 2,
    title: "Title 2",
    description: "Description 2",
  },
  {
    id: 3,
    title: "Title 3",
    description: "Description 3",
  },
  {
    id: 4,
    title: "Title 4",
    description: "Description 4",
  },
  {
    id: 5,
    title: "Title 5",
    description: "Description 5",
  },
  {
    id: 6,
    title: "Title 6",
    description: "Description 6",
  },
];

class App extends Component {
  static defaultProps = {
    name: "Carl",
  };

  state = {
    emails: [],
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({ emails: EMAILS });
    }, 0);
  }

  handleMessageAdd = (message) => {
    this.setState(({ emails }) => ({
      emails: [...emails, message],
    }));
  };

  handleMessageDelete = (index) => {
    this.setState(({ emails }) => ({
      emails: [...emails.slice(0, index), ...emails.slice(index + 1)],
    }));
  };

  render() {
    return (
      <>
        <Header />
        <Switch>
          <Redirect exact from="/" to="/home" />
          <Route
            exact
            path="/home"
            render={(props) => <Home {...props} name="Dave" />}
          />
          <Route
            path="/messages"
            render={(props) => <Messages {...props} messages={EMAILS} />}
          />
          <Route exact path="/sent" component={Sent} />
          <Route path="*" component={NotFound} />
        </Switch>
        <Footer />
      </>
    );
  }
}

export default App;
