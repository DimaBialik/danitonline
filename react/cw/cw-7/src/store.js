import { createStore } from "redux";

export const INCREMENT_AGE_ACTION = "INCREMENT_AGE_ACTION";
export const DECREMENT_AGE_ACTION = "DECREMENT_AGE_ACTION";

const initialState = {
  user: "James",
  age: 35,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT_AGE_ACTION:
      return { ...state, age: state.age + action.payload };
    case DECREMENT_AGE_ACTION:
      return { ...state, age: state.age - action.payload };
    default:
      return state;
  }
};

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
