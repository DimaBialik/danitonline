import React from "react";

function Form() {
  const [login, setLogin] = React.useState("");
  const [password, setPassword] = React.useState("");

  const handleChange = (event) => {
    const { name, value } = event.target;

    if (name === "login") {
      return setLogin(value);
    }

    if (name === "password") {
      return setPassword(value);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    console.log({ login, password });
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="email" name="login" value={login} onChange={handleChange} />
      <br />
      <input
        type="password"
        name="password"
        value={password}
        onChange={handleChange}
      />
      <button type="submit">Click</button>
    </form>
  );
}

export default Form;
