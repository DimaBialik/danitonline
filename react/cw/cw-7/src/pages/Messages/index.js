import React from "react";

import { Routes, Route } from "react-router-dom";

import NotFound from "../../pages/NotFound";

import Message from "./Message";
import MessageList from "./MessageList";

function Messages({ messages }) {
  return (
    <Routes>
      <Route path="/" element={<MessageList messages={messages} />} />
      <Route path="/:id" element={<Message messages={messages} />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

export default Messages;
