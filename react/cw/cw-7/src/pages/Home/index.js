import React from "react";

import { connect } from "react-redux";

import { INCREMENT_AGE_ACTION, DECREMENT_AGE_ACTION } from "../../store";

function Home({ user, age, incrementAge, decrementAge }) {
  const [diff, setDiff] = React.useState(5);

  const handleInputChange = (event) => {
    const { value } = event.target;

    setDiff(parseInt(value));
  };

  return (
    <>
      <h1>Hello, {user}</h1>
      <p>Age: {age}</p>
      <button type="button" onClick={() => incrementAge(diff)}>
        Increment age
      </button>
      <button type="button" onClick={() => decrementAge(diff)}>
        Decrement age
      </button>
      <input type="number" value={diff} onChange={handleInputChange} />
    </>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
  age: state.age,
});

const mapDispatchToProps = (dispatch) => ({
  incrementAge: (age) => dispatch({ type: INCREMENT_AGE_ACTION, payload: age }),
  decrementAge: (age) => dispatch({ type: DECREMENT_AGE_ACTION, payload: age }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
