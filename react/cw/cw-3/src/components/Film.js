import React from "react";
import FilmDescription from "./FilmDescription";

class Film extends React.Component{
    state = {
        open: false
    }
    onClick = () => {
        this.setState((prevState)=>{
            return {open: !prevState.open}
        })
    }
    render() {
        const {film} = this.props;
        return(
            <li>{film.name} <button onClick={this.onClick}>Детальнее</button>
                {this.state.open === true && <FilmDescription film={film}/>}
                </li>

        )
    }
}

export default Film;