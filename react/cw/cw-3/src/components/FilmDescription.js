import React from "react";
import Character from "./Character";

class FilmDescription extends React.Component {
    render() {
        const {film} = this.props;
        return (
            <>
                <h3>Episid Id: {film.episodeId}</h3>
                <p>{film.openingCrawl}</p>
                <ul>{film.characters.map((url, index)=>{
                    return (<Character key={index} url={url}/>)
                })}
                </ul>
            </>

        )
    }
}
export default FilmDescription;