import React from "react";
import Film from "./Film";

class Films extends React.Component {
    render() {
        const {films} = this.props;
        return (
            <ul>
                {films.map((film) => {
                    return (<Film key={film.id} film={film}/>)
                })
                }
            </ul>
        )
    }
}
export default Films