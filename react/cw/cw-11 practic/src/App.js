import {useDispatch} from "react-redux";
import React, {useContext} from "react";
import Films from "components/Films";
import {Redirect, Route, Switch} from "react-router-dom";
import FilmPage from "components/FilmPage";
import {fetchFilms} from "state/actions/films";
import Header from "./components/Header";
import Login from "./components/Login/Login";
import ThemeContext from "./context/theme/themeContext";

function App() {
    const {currentTheme} = useContext(ThemeContext);

    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(fetchFilms());
    }, [dispatch]);


    return (
        <div style={currentTheme}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet animi dicta dolore, eligendi explicabo fuga harum iusto minus neque nobis non officia placeat possimus quisquam, reprehenderit ut vero, voluptatibus.
            <Header/>
            <Switch>
                <Redirect exact from='/' to='/films'/>
                <Route exact path='/films' component={Films}>
                </Route>
                <Route path="/films/:filmId" component={FilmPage}/>
                <Route path="/login" component={Login}></Route>
            </Switch>
        </div>
    )
}


export default App;
