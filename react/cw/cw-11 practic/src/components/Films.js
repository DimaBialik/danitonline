import React, {useContext} from "react";
import {connect} from "react-redux";
import Film from "components/Film";


function Films({films, isLoading, isError, error}) {

	if (isLoading === true) {
		return <p>Loading....</p>;
	}

	if (isError === true) {
		return <p>{error.message}</p>;
	}

	if (films.length === 0) {
		return <p>No films found</p>
	}

	return (
			<ul>
				{films.map((film, index) => {
					return (
						<Film
							key={film.id}
							film={film}
						/>
					);
				})}
			</ul>
	);
}

const mapStateToProps = (state) => {
	return {
		films: state.films.items,
		isLoading: state.films.isLoading,
		isError: state.films.isError,
		error: state.films.error,
	}
}

export default connect(mapStateToProps)(Films);
