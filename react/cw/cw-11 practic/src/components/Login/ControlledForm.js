import {useState} from "react";

function ControlledForm({onSubmit}) {
    const [form, setForm] = useState({
        mail: "",
        password: ""
    })
    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(form);
    }
    const handleChange = (e) => {
        const {name, value} = e.target;
        setForm(prevState => {
            return {...prevState, [name]: value}
        })
    }

    return <form onSubmit={handleSubmit}>
        <input onChange={handleChange} name="mail" type="email" value={form.mail}
               placeholder="example@gmail.com"></input>
        <input onChange={handleChange} name="password" type="password"
               value={form.password}></input>
        <br/>
        <button type="submit">Submit</button>
    </form>
}

export default ControlledForm