import React from "react";
import Character from "components/Character";

function FilmDescription({ film }) {
  return (
    <>
      <h3>Episode Id: {film.episodeId}</h3>
      <p>{film.openingCrawl}</p>
      <ul>
        {film.characters.map((url, index) => {
          return <Character key={index} url={url}/>;
        })}
      </ul>
    </>
  );
}

export default FilmDescription;
