import {connect} from "react-redux";
import React from "react";
import Films from "./components/Films";
import {STORE_FILMS} from "./state/store";

function App({dispatch}) {
    const [films, setFilms] = React.useState([]);
    const [isLoading, setIsLoading] = React.useState(true);

    React.useEffect(() => {
        fetch("/api/movies", {method: "GET"})
            .then((res) => res.json())
            .then((data) => {
                dispatch({type: STORE_FILMS, payload: data})
                setIsLoading(false);
            })
            .catch((err) => console.error(err));
    }, []);

    if (isLoading === true) {
        return <p>Loading....</p>;
    }

    return (

        <Films/>
    )
}


export default connect()(App);
