import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import FilmPage from './components/FilmPage';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";
import {Switch, Route, Redirect} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./state/store";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <Switch>
                    <Redirect to='/films' from='/' exact/>
                    <Route exact path='/films'>
                        <App/>
                    </Route>
                    <Route path="/films/:filmId"
                           render={(props) =>
                               <FilmPage {...props}>Lololo</FilmPage>
                           }/>
                </Switch>

            </BrowserRouter>
        </Provider>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
