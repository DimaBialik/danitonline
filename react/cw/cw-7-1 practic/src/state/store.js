import {createStore} from "redux";

export const STORE_FILMS = "STORE_FILMS";
const initialState = {
    items: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case STORE_FILMS:
            return {...state, items: action.payload}
        default:
            return state
    }
}

const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
export default store