import React from "react";
import FilmDescription from "./FilmDescription";
import {Link} from "react-router-dom";

function Film({ film, onDelete }) {
  const [isOpen, setIsOpen] = React.useState(false);

  const toggleOpen = () => {
    setIsOpen((prevIsOpen) => !prevIsOpen);
  };

  React.useEffect(() => {
    console.log("Film.js useEffect");
  });

  return (
    <li>
      {film.name} <button onClick={toggleOpen}>More</button>
      <button onClick={onDelete}>X</button>
      {isOpen === true && <FilmDescription film={film} />}
    </li>
  );
}

export default Film;
