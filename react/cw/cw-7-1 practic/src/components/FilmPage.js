import {useState, useEffect} from "react";
import Film from "./Film";

function FilmPage({match}) {
    const [film, setFilm] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    useEffect(() => {
        fetch(`/api/movies/${match.params.filmId}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error("Response is not ok")
                }
                return response.json()
            })
            .then(data => {
                setFilm(data);
            })
            .catch(error => console.error(error))
            .finally(() => setIsLoading(false))
    }, [])
    // console.log(film);
    if (isLoading === true) {
        return <div>Is Loading...</div>
    }
    if (film === null) {
        return <div>Film is not found</div>
    }
    return <Film film={film}/>
}

export default FilmPage;