import React from 'react';

import Header from './components/Header';
import Footer from './components/Footer/Footer';
import Main from './components/Main';

import './App.css';

const EMAILS = [
    {
        id: 1,
        title: 'Title 1',
        description: 'Description 1'
    },
    {
        id: 2,
        title: 'Title 2',
        description: 'Description 2'
    },
    {
        id: 3,
        title: 'Title 3',
        description: 'Description 3'
    },
    {
        id: 4,
        title: 'Title 4',
        description: 'Description 4'
    },
    {
        id: 5,
        title: 'Title 5',
        description: 'Description 5'
    },
    {
        id: 6,
        title: 'Title 6',
        description: 'Description 6'
    }
];

function App() {
    const [emails, setEmails] = React.useState([]);

    React.useEffect(() => {
        setEmails(EMAILS);
    }, []);

    const handleMessageAdd = message => {
        setEmails(prevState => [...prevState, message]);
    };

    const handleMessageDelete = index => {
        // setEmails(({ emails }) => ({
        setEmails(prevState => [
            ...prevState.slice(0, index),
            ...prevState.slice(index + 1)
        ]);
    };

    return (
        <>
            <Header />
            <Main
                emails={emails}
                onMessageAdd={handleMessageAdd}
                onMessageDelete={handleMessageDelete}
            />
            <Footer />
        </>
    );
}

export default App;
