import React from 'react';

function Message({ email, onDelete }) {
    return (
        <React.Fragment>
            <h3>
                ({email.id}) {email.title}
            </h3>
            <p>{email.description}</p>
            <button type="button" onClick={onDelete}>
                Delete
            </button>
        </React.Fragment>
    );
}

export default Message;
