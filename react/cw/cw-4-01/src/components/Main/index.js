import React from 'react';

import Message from './Message';

function Main({ emails, onMessageAdd, onMessageDelete }) {
    return (
        <main>
            {emails.map((email, index) => (
                <Message
                    key={email.id}
                    email={email}
                    onDelete={() => onMessageDelete(index)}
                />
            ))}
            <div>
                <button
                    type="button"
                    onClick={() =>
                        onMessageAdd({
                            id: Date.now(),
                            title: `Title ${Date.now()}`,
                            description: `Description ${Date.now()}`
                        })
                    }
                >
                    Add
                </button>
            </div>
        </main>
    );
}

export default Main;
