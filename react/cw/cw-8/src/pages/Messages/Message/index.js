import React from 'react';

import { Link, useParams, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import NotFound from '../../../pages/NotFound';

function Message() {
    const { id } = useParams();
    const navigate = useNavigate();
    const messages = useSelector(state => state.emails.emails);

    const message = messages.find(m => m.id === parseInt(id));

    if (typeof message === 'undefined') {
        return <NotFound />;
    }

    const handleBackClick = () => {
        navigate(-1);
    };

    return (
        <div>
            <Link to="..">/Messages</Link> / {message.id} <br />
            <button type="button" onClick={handleBackClick}>
                Go back
            </button>
            <h3>
                ({message.id}) {message.title}
            </h3>
            <p>{message.description}</p>
        </div>
    );
}

export default Message;
