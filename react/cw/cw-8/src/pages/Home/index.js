import React from "react";

import { useSelector, useDispatch } from "react-redux";

import { incrementAge, decrementAge } from "../../state/actions/user";

function Home() {
  const user = useSelector((state) => state.user.name);
  const age = useSelector((state) => state.user.age);

  const dispatch = useDispatch();

  const [diff, setDiff] = React.useState(5);

  const handleInputChange = (event) => {
    const { value } = event.target;

    setDiff(parseInt(value));
  };

  const handleIncrementAge = () => {
    dispatch(incrementAge(diff));
  };

  const handleDecrementAge = () => {
    dispatch(decrementAge(diff));
  };

  return (
    <>
      <h1>Hello, {user}</h1>
      <p>Age: {age}</p>
      <button type="button" onClick={handleIncrementAge}>
        Increment age
      </button>
      <button type="button" onClick={handleDecrementAge}>
        Decrement age
      </button>
      <input type="number" value={diff} onChange={handleInputChange} />
    </>
  );
}

export default Home;
