export const STORE_EMAILS = "STORE_EMAILS";
export const ADD_EMAIL = "ADD_EMAIL";
export const DELETE_EMAIL = "DELETE_EMAIL";

export function storeEmails(value) {
  return {
    type: STORE_EMAILS,
    payload: value,
  };
}

export function addEmail(value) {
  return {
    type: ADD_EMAIL,
    payload: value,
  };
}

export function deleteEmail(id) {
  return {
    type: DELETE_EMAIL,
    payload: id,
  }
}
