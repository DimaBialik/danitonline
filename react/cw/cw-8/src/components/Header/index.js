import React from "react";

import { NavLink } from "react-router-dom";

import "./Header.css";

function Link(props) {
  const isLinkActive = ({ isActive }) => {
    return isActive === true ? "selected" : "";
  };

  return <NavLink {...props} className={isLinkActive} />;
}

function Header() {
  return (
    <header id="header">
      <ul>
        <li>
          <Link end to="/">
            Home
          </Link>
        </li>
        <li>
          <Link to="/messages">Messages</Link>
        </li>
        <li>
          <Link to="/sent">Sent</Link>
        </li>
      </ul>
    </header>
  );
}

export default Header;
