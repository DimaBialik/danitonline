import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk'
import logger from 'redux-logger';

import reducers from "./reducers";

import { myLogger } from './middleware/myLogger';

const store = createStore(
  reducers,
  applyMiddleware(thunk, logger, myLogger),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
