import React from "react";

function magic(value) {
  return Math.pow(Math.sqrt(value * Math.random()) / 5, 4);
}

function Counter({ counter }) {
  const list = React.useMemo(() => {
    return [1, 2, 3, 4, 5, 6, 7, 8].map(magic);
  }, []);

  return (
    <>
      {list.join("||||")}
      <div>{counter}</div>
    </>
  );
}

export default Counter;
