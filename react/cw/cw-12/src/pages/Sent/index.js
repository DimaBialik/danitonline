import React from "react";

import Body from "./Body";
import Counter from "./Counter";

function Sent() {
  const [counter, setCounter] = React.useState(0);

  const handleCounterClick = () => {
    setCounter((prevCounter) => prevCounter + 1);
  };

  return (
    <div>
      <Body description="Lorem ipsum" />
      <button type="button" onClick={handleCounterClick}>
        Click
      </button>
      <Counter counter={counter} />
    </div>
  );
}

export default Sent;
