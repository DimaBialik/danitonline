import React from "react";

function Body({ description }) {
  return <p>{description}</p>;
}

export default React.memo(Body);
