import React from "react";

import { Link } from "react-router-dom";

import MailContext from "context/MailContext";

function MessageListContext() {
  return (
    <MailContext.Consumer>
      {(messages) => (
        <>
          <h1>MessageListContext</h1>
          {messages.map((message) => (
            <div key={message.id}>
              <h3>
                <Link to={`${message.id}`}>
                  ({message.id}) {message.title}
                </Link>
              </h3>
            </div>
          ))}
        </>
      )}
    </MailContext.Consumer>
  );
}

export default MessageListContext;
