import React from "react";

import { Link } from "react-router-dom";

const ADD_EMAIL = "ADD_EMAIL";
const DELETE_EMAIL = "DELETE_EMAIL";

const EMAILS = [
  {
    id: 200,
    title: "Title 200",
    description: "Description 200",
  },
  {
    id: 201,
    title: "Title 101",
    description: "Description 201",
  },
  {
    id: 202,
    title: "Title 202",
    description: "Description 202",
  },
];

const reducer = (state, action) => {
  switch (action.type) {
    case ADD_EMAIL:
      return [...state, { id: Date.now(), ...action.payload }];
    case DELETE_EMAIL:
      return state.filter((email) => email.id !== action.payload);
    default:
      return state;
  }
};

function MessageList() {
  const [form, setForm] = React.useState({
    title: "",
    description: "",
  });
  const [messages, dispatch] = React.useReducer(reducer, EMAILS);

  const handleChange = (event) => {
    const { name, value } = event.target;

    setForm((prevForm) => ({ ...prevForm, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (form.title === "" || form.description === "") {
      return;
    }

    dispatch({
      type: ADD_EMAIL,
      payload: { title: form.title, description: form.description },
    });
  };

  const items = messages.map((message) => (
    <div key={message.id}>
      <h3>
        <Link to={`${message.id}`}>
          ({message.id}) {message.title}
        </Link>
      </h3>
      <p>{message.description}</p>
      <button
        type="button"
        onClick={() => dispatch({ type: DELETE_EMAIL, payload: message.id })}
      >
        X
      </button>
    </div>
  ));

  return (
    <>
      {items}
      <form onSubmit={handleSubmit}>
        <div>
          <input
            type="text"
            name="title"
            value={form.title}
            onChange={handleChange}
          />
        </div>
        <div>
          <textarea
            name="description"
            value={form.description}
            onChange={handleChange}
          />
        </div>
        <button type="submit">Send</button>
      </form>
    </>
  );
}

export default MessageList;
