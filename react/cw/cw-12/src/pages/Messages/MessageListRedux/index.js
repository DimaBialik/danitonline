import React from "react";

import { useSelector, useDispatch } from "react-redux";

import { Link } from "react-router-dom";

import { deleteEmail } from "state/actions/emails";

function MessageListRedux() {
  const { items, error, isFetching, isError } = useSelector(
    (state) => state.emails
  );

  const dispatch = useDispatch();

  if (isFetching === true) {
    return <h1>Loading...</h1>;
  }

  if (isError === true) {
    return (
      <>
        <h1>Oops, something went wrong</h1>
        <p>{error.message}</p>
      </>
    );
  }

  if (items.length === 0) {
    return <h1>Nothing found</h1>;
  }

  return items.map((message) => (
    <div key={message.id}>
      <h3>
        <Link to={`${message.id}`}>
          ({message.id}) {message.title}
        </Link>
      </h3>
      <p>{message.description}</p>
      <button type="button" onClick={() => dispatch(deleteEmail(message.id))}>
        X
      </button>
      <hr />
    </div>
  ));
}

export default MessageListRedux;
