import React from "react";

import { Routes, Route } from "react-router-dom";

import MailContext from "context/MailContext";

import NotFound from "pages/NotFound";

import Message from "./Message";
// import MessageListRedux from "./MessageListRedux";
// import MessageListContext from "./MessageListContext";
import MessageList from "./MessageList";

const EMAILS = [
  {
    id: 1,
    title: "Title 1",
    description: "Description 1",
  },
  {
    id: 2,
    title: "Title 2",
    description: "Description 2",
  },
  {
    id: 3,
    title: "Title 3",
    description: "Description 3",
  },
  {
    id: 4,
    title: "Title 4",
    description: "Description 4",
  },
];

function Messages() {
  return (
    <MailContext.Provider value={EMAILS}>
      <Routes>
        {/* <Route path="/" element={<MessageListRedux />} /> */}
        {/* <Route path="/" element={<MessageListContext />} /> */}
        <Route path="/" element={<MessageList />} />
        <Route path="/:id" element={<Message />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </MailContext.Provider>
  );
}

export default Messages;
