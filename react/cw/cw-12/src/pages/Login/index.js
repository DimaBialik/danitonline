import React from "react";

import ControlledForm from "./ControlledForm";
// import UncontrolledForm from "./UncontrolledForm";
// import FormikForm from "./FormikForm";

function Login() {
  const handleSubmit = (data) => {
    fetch("https://api.domain.com", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    });
  };

  return <ControlledForm onSubmit={handleSubmit} />;
  // return <UncontrolledForm onSubmit={handleSubmit} />;

  // return <FormikForm onSubmit={handleSubmit} />;
}

export default Login;
