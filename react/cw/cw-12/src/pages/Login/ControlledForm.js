import React from "react";

import usePasswordType from "hooks/usePasswordType";

const EMAIL_REGEX =
  /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

function ControlledForm({ onSubmit }) {
  const [state, setState] = React.useState({
    login: "my@email.com",
    password: "",
  });
  const [errors, setErrors] = React.useState({});

  const [inputType, changeInputType] = usePasswordType();

  const handleBlur = (event) => {
    const { name, value } = event.target;

    let validationErrors = {};

    if (EMAIL_REGEX.test(value) === false) {
      validationErrors.login = "Login is invalid";
    }

    if (value === "") {
      validationErrors.login = "Login is required";
    }

    setErrors((prevValidationErrors) => ({
      ...prevValidationErrors,
      ...validationErrors,
    }));
  };

  const handleChange = (event) => {
    const { name, value } = event.target;

    setState((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    let validationErrors = {};

    setErrors(validationErrors);

    if (EMAIL_REGEX.test(state.login) === false) {
      validationErrors.login = "Login is invalid";
    }

    if (state.login === "") {
      validationErrors.login = "Login is required";
    }

    if (state.password === "") {
      validationErrors.password = "Password is required";
    }

    if (Object.keys(validationErrors).length > 0) {
      return setErrors(validationErrors);
    }

    onSubmit(state);
  };

  const handleReset = () => {
    setState({
      login: "my@email.com",
      password: "",
    });
  };

  return (
    <form onSubmit={handleSubmit} onReset={handleReset}>
      <div>Controlled form</div>
      <div>
        <input
          type="email"
          name="login"
          value={state.login}
          onBlur={handleBlur}
          onChange={handleChange}
        />
        <p style={{ color: "#ff0000" }}>{errors.login ? errors.login : null}</p>
      </div>
      <div>
        <input
          type={inputType}
          name="password"
          value={state.password}
          onChange={handleChange}
        />
        <button type="button" onClick={changeInputType}>
          {inputType === "password" ? "Show" : "Hide"}
        </button>
        <p style={{ color: "#ff0000" }}>
          {errors.password ? errors.password : null}
        </p>
      </div>
      <button type="submit">Submit</button>
      <button type="reset">Reset</button>
    </form>
  );
}

export default ControlledForm;
