import { createContext } from 'react';

const EMAILS = [
  {
    id: 100,
    title: "Title 100",
    description: "Description 100"
  },
  {
    id: 101,
    title: "Title 101",
    description: "Description 101"
  },
  {
    id: 102,
    title: "Title 102",
    description: "Description 102"
  }
]

const MailContext = createContext(EMAILS);

export default MailContext;
