import React from "react";

function usePasswordType() {
  const [inputType, setInputType] = React.useState("password");

  const changeInputType = () => {
    setInputType(prevInputType => prevInputType === "password" ? "text" : "password");
  }

  return [inputType, changeInputType];
}

export default usePasswordType;
