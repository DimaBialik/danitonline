import React, { useState, useEffect } from 'react';
import Films from './components/Films';

// class App extends React.Component {
//     state = {
//         films: [],
//         isLoading: true
//     };

//     componentDidMount() {
//         fetch('https://ajax.test-danit.com/api/swapi/films', {
//             method: 'GET'
//         })
//             .then(response => response.json())
//             .then(json => this.setState({ films: json, isLoading: false }));
//     }
//     render() {
//         console.log(this.state.films);
//         if (this.state.isLoading === true) {
//         }
//         return <Films films={this.state.films} />;
//     }
// }
function App() {
    const [films, setFilms] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    useEffect(() => {
        console.log('APP js componentDidMount');
        fetch('https://ajax.test-danit.com/api/swapi/films', {
            method: 'GET'
        })
            .then(response => response.json())
            .then(data => {
                setFilms(data);
                setIsLoading(false);
            });
    }, [isLoading]);
    if (isLoading === true) {
        return <p>Loading....</p>;
    }
    return <Films films={films} />;
}

export default App;
