import React from 'react';
import FilmDescription from './FilmDescription';

function Film({ film }) {
    const [isOpen, setIsOpen] = React.useState(false);
    const toggleOpen = () => {
        setIsOpen(prevIsOpen => !prevIsOpen);
    };
    React.useEffect(() => {
        console.log('films is change');
    });

    return (
        <li>
            {film.name} <button onClick={toggleOpen}>More</button>
            {isOpen === true && <FilmDescription film={film} />}
        </li>
    );
}

export default Film;
