import React from 'react';

class Character extends React.Component {
    state = {
        character: {},
        isLoading: true
    };
    componentDidMount() {
        fetch(this.props.url, {
            method: 'GET'
        })
            .then(response => response.json())
            .then(json => this.setState({ character: json, isLoading: false }));
    }
    render() {
        if (this.state.isLoading === true) {
            return null;
        }
        return <li>{this.state.character.name}</li>;
    }
}

export default Character;
