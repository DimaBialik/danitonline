import React from 'react';
import Film from './Film';

function Films({ films }) {
    return (
        <ul>
            {films.map(film => {
                return <Film key={film.id} film={film} />;
            })}
        </ul>
    );
}
export default Films;
