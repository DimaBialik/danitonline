import React from "react";

import { Link } from "react-router-dom";

function MessageList({ messages }) {
  return messages.map((message) => (
    <div key={message.id}>
      <h3>
        <Link to={`/messages/${message.id}`}>
          ({message.id}) {message.title}
        </Link>
      </h3>
      <p>{message.description}</p>
      <hr />
    </div>
  ));
}

export default MessageList;
