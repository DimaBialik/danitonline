import React from 'react';

import { Link, useParams, useNavigate } from 'react-router-dom';

import NotFound from '../../../pages/NotFound';

function Message({ messages }) {
    const { id } = useParams();
    const navigate = useNavigate();

    const message = messages.find(m => m.id === parseInt(id));

    if (typeof message === 'undefined') {
        return <NotFound />;
    }

    const handleBackClick = () => {
        navigate(-1);
    };

    return (
        <div>
            <Link to="/messages">/Messages</Link> / {message.id} <br />
            <button type="button" onClick={handleBackClick}>
                Go back
            </button>
            <h3>
                ({message.id}) {message.title}
            </h3>
            <p>{message.description}</p>
        </div>
    );
}

export default Message;
