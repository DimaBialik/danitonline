import React from 'react';

function Login({ onLogin }) {
    const handleSubmit = e => {
        e.preventDefault();
        onLogin({ userName: 'JohnDoe' });
    };
    return (
        <>
            <form action="" onChange={handleSubmit}>
                <input type="text" name="login" />
                <input type="password" name="password" />
                <button type="submit"></button>
            </form>
        </>
    );
}

export default Login;
