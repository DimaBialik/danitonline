import React from "react";

import { NavLink } from "react-router-dom";

import './Header.css';

function Header() {
  return (
    <header id="header">
      <ul>
        <li>
          <NavLink exact to="/" activeClassName="selected">Home</NavLink>
        </li>
        <li>
          <NavLink to="/messages" activeClassName="selected">Messages</NavLink>
        </li>
        <li>
          <NavLink to="/sent" activeClassName="selected">Sent</NavLink>
        </li>
      </ul>
    </header>
  );
}

export default Header;
