import React, { Component } from 'react';

import Header from './components/Header';
import Footer from './components/Footer/Footer';

import Home from './pages/Home';
import Messages from './pages/Messages';
import Sent from './pages/Sent';
import NotFound from './pages/NotFound';
import Login from './pages/Login';
import ProtectedRoute from './components/ProtectedRoute';
import { Routes, Route, Navigate } from 'react-router-dom';

import './App.css';

const EMAILS = [
    {
        id: 1,
        title: 'Title 1',
        description: 'Description 1'
    },
    {
        id: 2,
        title: 'Title 2',
        description: 'Description 2'
    },
    {
        id: 3,
        title: 'Title 3',
        description: 'Description 3'
    },
    {
        id: 4,
        title: 'Title 4',
        description: 'Description 4'
    },
    {
        id: 5,
        title: 'Title 5',
        description: 'Description 5'
    },
    {
        id: 6,
        title: 'Title 6',
        description: 'Description 6'
    }
];

class App extends Component {
    static defaultProps = {
        name: 'Carl'
    };

    state = {
        emails: [],
        user: null
    };

    componentDidMount() {
        setTimeout(() => {
            this.setState({ emails: EMAILS });
        }, 0);
    }

    handleMessageAdd = message => {
        this.setState(({ emails }) => ({
            emails: [...emails, message]
        }));
    };

    handleMessageDelete = index => {
        this.setState(({ emails }) => ({
            emails: [...emails.slice(0, index), ...emails.slice(index + 1)]
        }));
    };
    handleLogin = user => {
        this.setState({ user: user });
    };

    render() {
        return (
            <>
                <Header />
                <Routes>
                    <Route path="/" element={<Navigate to="/home" />} />
                    <Route path="/home" element={<Home name="Dave" />} />
                    <Route
                        path="/messages"
                        element={<Messages messages={EMAILS} />}
                    />
                    <Route
                        path="/sent"
                        element={
                            <ProtectedRoute user={this.state.user}>
                                <Sent />
                            </ProtectedRoute>
                        }
                    />
                    <Route path="*" element={<NotFound />} />
                    <Route
                        path="/Login"
                        element={<Login onLogin={this.handleLogin} />}
                    />
                </Routes>
                <Footer />
            </>
        );
    }
}

export default App;
