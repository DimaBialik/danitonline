import React from "react";

import { Routes, Route } from "react-router-dom";

import NotFound from "../../pages/NotFound";

import Message from "./Message";
import MessageList from "./MessageList";

function Messages() {
  return (
    <Routes>
      <Route path="/" element={<MessageList />} />
      <Route path="/:id" element={<Message />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

export default Messages;
