import React from 'react';

import { useSelector, useDispatch } from 'react-redux';

import { Link } from 'react-router-dom';

import { deleteEmail } from '../../../state/actions/emails';

function MessageList() {
    const messages = useSelector(state => state.emails.emails);

    const dispatch = useDispatch();

    return messages.map(message => (
        <div key={message.id}>
            <h3>
                <Link to={`${message.id}`}>
                    ({message.id}) {message.title}
                </Link>
            </h3>
            <p>{message.description}</p>
            <button
                type="button"
                onClick={() => dispatch(deleteEmail(message.id))}
            >
                X
            </button>
            <hr />
        </div>
    ));
}

export default MessageList;
