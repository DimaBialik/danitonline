import { STORE_EMAILS, ADD_EMAIL, DELETE_EMAIL } from "../actions/emails";

const initialState = {
  emails: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case STORE_EMAILS:
      return {
        ...state,
        emails: action.payload,
      };
    case ADD_EMAIL:
      return {
        ...state,
        emails: [...state.emails, action.payload],
      };
    case DELETE_EMAIL:
      return {
        ...state,
        emails: state.emails.filter((email) => email.id !== action.payload),
      };
    default:
      return state;
  }
};

export default reducer;
