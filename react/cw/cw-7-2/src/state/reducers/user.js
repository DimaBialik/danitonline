import { INCREMENT_AGE, DECREMENT_AGE } from "../actions/user";

const initialState = {
  name: "Dave",
  age: 35,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT_AGE:
      return { ...state, age: state.age + action.payload };
    case DECREMENT_AGE:
      return { ...state, age: state.age - action.payload };
    default:
      return state;
  }
};

export default reducer;
