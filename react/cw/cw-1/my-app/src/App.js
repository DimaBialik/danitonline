import React from 'react';
import Button from './components/Button/Button';
import './App.css';
import Counter from './components/Counter/Counter';

class App extends React.Component {
    state = {
        counter: 50
    };

    addValue = () => {
        this.setState(({ counter }) => {
            return {
                counter: counter < 100 ? counter + 10 : 100
            };
        });
    };

    minusValue = () => {
        this.setState(({ counter }) => {
            return {
                counter: counter > 0 ? counter - 10 : 0
            };
        });
    };

    render() {
        const { counter } = this.state;
        return (
            <div className="App">
                <Button onClick={this.addValue}>+</Button>
                <Counter counter={counter} />
                <Button onClick={this.minusValue}>-</Button>
            </div>
        );
    }
}

export default App;
