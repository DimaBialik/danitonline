import {useDispatch} from "react-redux";
import React from "react";
import Films from "components/Films";
import {Redirect, Route, Switch} from "react-router-dom";
import FilmPage from "components/FilmPage";
import {fetchFilms} from "state/actions/films";

function App() {
	const dispatch = useDispatch();

	React.useEffect(() => {
		dispatch(fetchFilms());
	}, [dispatch]);


	return (
		<Switch>
			<Redirect exact from='/' to='/films'/>
			<Route exact path='/films' component={Films}>
			</Route>
			<Route path="/films/:filmId" component={FilmPage}/>
		</Switch>
	)
}


export default App;
