import React, { Component } from "react";

import { connect } from "react-redux";

import Header from "./components/Header";
import Footer from "./components/Footer/Footer";

import Home from "./pages/Home";
import Messages from "./pages/Messages";
import Sent from "./pages/Sent";
import Login from "./pages/Login";

import { fetchEmails } from "./state/actions/emails";

import { Routes, Route, Navigate } from "react-router-dom";

import "./App.css";

class App extends Component {
  componentDidMount() {
    this.props.dispatch(fetchEmails());
  }

  render() {
    return (
      <>
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/messages/*" element={<Messages />} />
          <Route path="/sent" element={<Sent />} />
          <Route path="/login" element={<Login />} />
          <Route path="*" element={<Navigate to="/" />} />
        </Routes>
        <Footer />
      </>
    );
  }
}

export default connect()(App);
