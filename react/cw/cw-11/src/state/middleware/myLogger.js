export const myLogger = (store) => (next) => (action) => {
  console.log(`Событие: ${action.type}, payload данные:`, JSON.stringify(action.payload, undefined, 2));
  return next(action);
};
