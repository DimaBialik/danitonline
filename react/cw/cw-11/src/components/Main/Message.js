import React from "react";

class Message extends React.Component {
  state = {
    tick: new Date(),
  };

  intervalId = null;

  componentDidMount() {
    this.intervalId = setInterval(() => {
      this.setState({ tick: new Date() });
    }, 1000);
  }

  handlePageClick = () => {
    this.setState({ page: this.state.page + 1 });
  };

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  render() {
    const { email, onDelete } = this.props;
    const { tick } = this.state;

    return (
      <React.Fragment>
        <h3>
          ({email.id}) {email.title}
        </h3>
        <p>{email.description}</p>
        <button type="button" onClick={onDelete}>
          Delete
        </button>
        <span>{tick.toLocaleTimeString()}</span>
      </React.Fragment>
    );
  }
}

export default Message;
