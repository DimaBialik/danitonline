import React from 'react';

import { Formik, useFormik } from 'formik';
import * as Yup from 'yup';

const Schema = Yup.object().shape({
    login: Yup.string().required('Required').email(),
    password: Yup.string()
        .required('Required')
        .min(6, 'Too Short)')
        .max(9, 'Too Long)')
});

// function FormikForm() {
//   return (
//     <Formik
//       initialValues={{
//         login: "",
//         password: "",
//       }}
//       validationSchema={Schema}
//       onSubmit={(values) => console.log(values)}
//     >
//       {({ values, errors, handleChange, handleSubmit }) => (
//         <form onSubmit={handleSubmit}>
// <div>Formik form</div>
// <div>
//   <input
//     type="text"
//     name="login"
//     value={values.login}
//     onChange={handleChange}
//   />
//   <p style={{ color: "#ff0000" }}>
//     {errors.login ? errors.login : null}
//   </p>
// </div>
// <div>
//   <input
//     type="password"
//     name="password"
//     value={values.password}
//     onChange={handleChange}
//   />
//   <p style={{ color: "#ff0000" }}>
//     {errors.password ? errors.password : null}
//   </p>
// </div>
// <button type="submit">Submit</button>
// <button type="reset">Reset</button>
//         </form>
//       )}
//     </Formik>
//   );
// }

function FormikForm({ onSubmit }) {
    const formik = useFormik({
        initialValues: {
            login: '',
            password: ''
        },
        validationSchema: Schema,
        onSubmit(values) {
            onSubmit(values);
        }
    });

    return (
        <form onSubmit={formik.handleSubmit}>
            <div>Formik form</div>
            <div>
                <input
                    type="text"
                    name="login"
                    value={formik.values.login}
                    onChange={formik.handleChange}
                />
                <p style={{ color: '#ff0000' }}>
                    {formik.errors.login ? formik.errors.login : null}
                </p>
            </div>
            <div>
                <input
                    type="password"
                    name="password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                />
                <p style={{ color: '#ff0000' }}>
                    {formik.errors.password ? formik.errors.password : null}
                </p>
            </div>
            <button type="submit">Submit</button>
            <button type="reset">Reset</button>
        </form>
    );
}

export default FormikForm;
