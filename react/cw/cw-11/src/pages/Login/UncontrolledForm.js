import React from "react";

function UncontrolledForm() {
  const loginRef = React.useRef(null);
  const passwordRef = React.useRef(null);

  const handleSubmit = (event) => {
    event.preventDefault();

    console.log(loginRef.current.value);
    console.log(passwordRef.current.value);
  };

  const handleReset = () => {};

  return (
    <form onSubmit={handleSubmit} onReset={handleReset}>
      <div>Uncontrolled form</div>
      <div>
        <input ref={loginRef} defaultValue="me@gmail.com" type="email" name="login" />
        {/* <p style={{ color: "#ff0000" }}>{errors.login ? errors.login : null}</p> */}
      </div>
      <div>
        <input ref={passwordRef} type="password" name="password" />
        {/* <p style={{ color: "#ff0000" }}>
          {errors.password ? errors.password : null}
        </p> */}
      </div>
      <button type="submit">Submit</button>
      <button type="reset">Reset</button>
    </form>
  );
}

export default UncontrolledForm;
