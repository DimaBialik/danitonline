export const INCREMENT_AGE = "INCREMENT_AGE";
export const DECREMENT_AGE = "DECREMENT_AGE";

export function incrementAge(value) {
  return {
    type: INCREMENT_AGE,
    payload: value,
  };
}

export function decrementAge(value) {
  return {
    type: DECREMENT_AGE,
    payload: value,
  };
}
