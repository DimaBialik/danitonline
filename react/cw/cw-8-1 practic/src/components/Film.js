import React from "react";
import FilmDescription from "./FilmDescription";
import {Link} from "react-router-dom";
import {useSelector, useDispatch} from "react-redux";

function Film({ film, onDelete }) {
  const [isOpen, setIsOpen] = React.useState(false);

  const toggleOpen = () => {
    setIsOpen((prevIsOpen) => !prevIsOpen);
  };


  return (
    <li>
      <Link to={`/films/${film.id}`}>{film.name}</Link>
      <button onClick={toggleOpen}>Show more</button>
      {isOpen === true && <FilmDescription film={film} />}
    </li>
  );
}

export default Film;
