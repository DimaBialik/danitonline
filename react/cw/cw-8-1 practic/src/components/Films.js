import React from "react";
import {connect} from "react-redux";
import Film from "./Film";

function Films({films}) {

    return (
        <ul>
            {films.map((film, index) => {
                return (
                    <Film
                        key={film.id}
                        film={film}
                    />
                );
            })}
        </ul>
    );
}

const mapStateToProps = (state) => {
    return {
        films: state.films.items
    }
}

export default connect(mapStateToProps)(Films);
