import {connect} from "react-redux";
import React from "react";
import Films from "./components/Films";
import {STORE_FILMS} from "./state/films";
import {Redirect, Route, Switch} from "react-router-dom";
import FilmPage from "./components/FilmPage";

function App({dispatch}) {
    const [isLoading, setIsLoading] = React.useState(true);

    React.useEffect(() => {
        fetch("/api/movies", {method: "GET"})
            .then((res) => res.json())
            .then((data) => {
                dispatch({type: STORE_FILMS, payload: data})
                setIsLoading(false);
            })
            .catch((err) => console.error(err));
    }, []);

    if (isLoading === true) {
        return <p>Loading....</p>;
    }
    return (
        <Switch>
            <Redirect exact from='/' to='/films'/>
            <Route exact path='/films' component={Films}>
            </Route>
            <Route path="/films/:filmId" component={FilmPage}/>
        </Switch>
    )
}


export default connect()(App);
