import { ADD_EMAIL, DELETE_EMAIL } from "../actions/emails";
import {
  FETCH_EMAILS_BEGIN,
  FETCH_EMAILS_SUCCESS,
  FETCH_EMAILS_FAILURE,
} from "../actions/emails";

const initialState = {
  items: [],
  isFetching: false,
  isError: false,
  error: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_EMAIL:
      return {
        ...state,
        items: [...state.items, action.payload],
      };
    case DELETE_EMAIL:
      return {
        ...state,
        items: state.items.filter((email) => email.id !== action.payload),
      };
    case FETCH_EMAILS_BEGIN:
      return {
        ...state,
        isFetching: true,
      };
    case FETCH_EMAILS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        items: action.payload,
      };
    case FETCH_EMAILS_FAILURE:
      return {
        ...state,
        isFetching: false,
        isError: true,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
