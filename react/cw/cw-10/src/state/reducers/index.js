import { combineReducers } from "redux";

import userReducer from './user';
import emailsReducer from './emails';

export default combineReducers({
  user: userReducer,
  emails: emailsReducer,
});
