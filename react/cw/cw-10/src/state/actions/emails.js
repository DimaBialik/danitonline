export const ADD_EMAIL = "ADD_EMAIL";
export const DELETE_EMAIL = "DELETE_EMAIL";

export function addEmail(value) {
  return {
    type: ADD_EMAIL,
    payload: value,
  };
}

export function deleteEmail(id) {
  return {
    type: DELETE_EMAIL,
    payload: id,
  };
}

function httpRequest() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const rand = Math.random();

      if (rand > 0.5) {
        return reject(new Error("HTTP Request Error"));
      }

      resolve([
        {
          id: 1,
          title: "Title 1",
          description: "Description 1",
        },
        {
          id: 2,
          title: "Title 2",
          description: "Description 2",
        },
        {
          id: 3,
          title: "Title 3",
          description: "Description 3",
        },
        {
          id: 4,
          title: "Title 4",
          description: "Description 4",
        },
        {
          id: 5,
          title: "Title 5",
          description: "Description 5",
        },
        {
          id: 6,
          title: "Title 6",
          description: "Description 6",
        },
      ]);
    }, 1000);
  });
}

export const FETCH_EMAILS_BEGIN = "FETCH_EMAILS_BEGIN";
export const FETCH_EMAILS_SUCCESS = "FETCH_EMAILS_SUCCESS";
export const FETCH_EMAILS_FAILURE = "FETCH_EMAILS_FAILURE";

export function fetchEmailsBegin() {
  return {
    type: FETCH_EMAILS_BEGIN,
  };
}

export function fetchEmailsSuccess(data) {
  return {
    type: FETCH_EMAILS_SUCCESS,
    payload: data,
  };
}

export function fetchEmailsFailure(error) {
  return {
    type: FETCH_EMAILS_FAILURE,
    payload: error,
  };
}

export function fetchEmails() {
  return (dispatch) => {
    dispatch(fetchEmailsBegin());

    httpRequest()
      .then((data) => dispatch(fetchEmailsSuccess(data)))
      .catch((error) => dispatch(fetchEmailsFailure(error)));
  };
}
